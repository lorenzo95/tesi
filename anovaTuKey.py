# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 17:09:28 2021

@author: Lorenzo
"""
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import scipy.stats as stats
import statsmodels.api as sm
from statsmodels.formula.api import ols
from bioinfokit.analys import stat
import moduleRead as mR

val_vars = mR.readFile()
# load data file
df = pd.read_csv('OpenCSVWriteByResultSet0.csv', sep=',')
df_melt = pd.melt(df.reset_index(), id_vars=['index'], value_vars=val_vars)
# replace column names
df_melt.columns = ['index', 'measure', 'value']

ax = sns.swarmplot(x='misure', y='value', data=df_melt, color='#7d0013')
plt.show()

# stats f_oneway functions takes the groups as input and returns F and P-value
fvalue, pvalue = stats.f_oneway(df[val_vars[0]], df[val_vars[1]])

mR.fileClean()
mR.writeFile('fvalue ', 'pvalue ', fvalue, pvalue)

# Ordinary Least Squares (OLS) model
model = ols('value ~ C(measure)', data=df_melt).fit()
anova_table = sm.stats.anova_lm(model, typ=2)
anova_table

# ANOVA table using bioinfokit v1.0.3 or later (it uses wrapper script for anova_lm)
res = stat()
res.tukey_hsd(df=df_melt, res_var='value', xfac_var='measure', anova_model='value ~ C(measure)')
res.tukey_summary

# QQ-plot
# res.anova_std_residuals are standardized residuals obtained from ANOVA (check above)
sm.qqplot(res.anova_std_residuals, line='45') # line='45' | 'q' | 'r'
plt.xlabel('Theoretical Quantiles')
plt.ylabel('Standardized Residuals')
plt.show()

# histogram
plt.hist(res.anova_model_out.resid, bins='auto', histtype='bar', ec='k') 
plt.xlabel('Residuals')
plt.ylabel('Frequency')
plt.show()