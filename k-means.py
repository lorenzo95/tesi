# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 10:05:15 2020

@author: Lorenzo
"""
import matplotlib.pyplot as mtp    
import pandas as pd  
import sys

# Importing the dataset  
dataset = pd.read_csv('OpenCSVWriteByResultSet0.csv') 
x = dataset.values

file = open('OpenCSVWriteByResultSet0.csv','r')
line = file.readline()
spl = line.split(",")
file.close()

piulinee = False
if (len(spl) > 2):
    piulinee = True

#finding optimal number of clusters using the elbow method  
from sklearn.cluster import KMeans  
wcss_list=[]  #Initializing the list for the values of WCSS  

#Using for loop for iterations from 1 to 10.  
for i in range(1,11):  
    kmeans = KMeans(n_clusters=i, init='k-means++', random_state=42)  
    if (piulinee):
        kmeans.fit(x[:,[0,1]])
    else :
        kmeans.fit(x[:,[0]])
    wcss_list.append(kmeans.inertia_)  
mtp.plot(range(1,11),wcss_list)  
mtp.title('The Elbow Method Graph')  
mtp.xlabel('Number of clusters(k)')  
mtp.ylabel('wcss_list')  
mtp.show()  

numCl = sys.argv[1]
n_clusters = 3
if (numCl is not None):
	n_clusters = int(numCl)
if (n_clusters < 2):
	n_clusters = 3
if (n_clusters > 5):
	n_clusters = 5
	
#training the K-means model on a dataset  
kmeans = KMeans(n_clusters,init='k-means++',random_state=42)  

if (piulinee):
    y_predict= kmeans.fit_predict(x[:,[0,1]])
else:
    y_predict= kmeans.fit_predict(x[:,[0]])  

#visulaizing the clusters 
colorString = ['blue','red','green']

if (len(colorString) < n_clusters) :
	colorString.append('purple')
if (len(colorString) < n_clusters) :
	colorString.append('cyan')

for cluster in range(0,n_clusters):
    color = colorString[cluster]
    mtp.scatter(x[y_predict == cluster,0], x[y_predict == cluster,1], s=60, c=color, label='Cluster '+str(cluster+1)) #for first cluster  

if (piulinee):
    mtp.scatter(kmeans.cluster_centers_[:,0], kmeans.cluster_centers_[:,1], s=100, c='yellow', label='Centroid')
else:
    mtp.scatter(kmeans.cluster_centers_[:,0], kmeans.cluster_centers_[:,0], s=100, c='yellow', label='Centroid')
#mtp.title('Clusters of sell')  
mtp.title('Clusters')  
mtp.xlabel(spl[0])
if (spl[1] is not None):
	mtp.ylabel(spl[1])
else:
	mtp.ylabel(spl[0])

if (n_clusters > 3):
	#mtp.legend(bbox_to_anchor =(1.17, 1.17), ncol = 2, loc='upper right')
	mtp.legend(bbox_to_anchor =(1.15, 1.19), ncol = 2, loc='upper right')
	#(bbox_to_anchor=(1.05, 1.0), loc='upper left')
else:
	mtp.legend(bbox_to_anchor =(1.15, 1.15), ncol = 2, loc='upper right')
mtp.show()  

fileClean = open('Dati_Out0.txt','w')
fileClean.write('')
fileClean.close()

file = open("Dati_Out0.txt","a")

for cluster in range(0,n_clusters):
    vect = x[y_predict == cluster]   
    vectSize = int(vect.size / (vect.ndim + 1)) # 1 per il group by
    
    file.write('Cluster ' + str(cluster) + ': ')
    for pos in range(0, vectSize):
        st = str(vect[pos][vect[pos].size - 1])
        if (pos < (vectSize - 1)):
            st += ';'
        file.write(st)
    file.write('\n')
file.close()