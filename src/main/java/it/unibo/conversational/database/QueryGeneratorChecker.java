package it.unibo.conversational.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import it.unibo.conversational.Utils;
import it.unibo.conversational.algorithms.utils.TabTable;
import it.unibo.conversational.algorithms.utils.TableTypes;
//import it.unibo.conversational.algorithms.Parser.Type;
import it.unibo.conversational.algorithms.utils.Type;
import it.unibo.conversational.datatypes.Entity;
import it.unibo.conversational.datatypes.Ngram;

/**
 * Interacting with the database SQL query.
 */
public final class QueryGeneratorChecker extends DBmanager {

  private QueryGeneratorChecker() {
  }

  /**
   * @return get the fact
   */
  public static Pair<Integer, String> getFactTable() {
    final String query = "SELECT * FROM `" + TabTable.tabTABLE + "` WHERE `" + type(TabTable.tabTABLE) + "` = \"" + TableTypes.FT + "\"";
    try (
        Statement stmt = getMetaConnection().createStatement();
        ResultSet res = stmt.executeQuery(query)
    ) {
      res.first();
      return Pair.of(res.getInt(id(TabTable.tabTABLE)), res.getString(name(TabTable.tabTABLE)));
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * @param m member
   * @return get the level of the given member
   */
  public static Ngram getLevelOfMember(final Ngram m) {
    final String query = "SELECT * FROM `" + TabTable.tabLEVEL + "` L, `" + TabTable.tabCOLUMN + "` C WHERE C." + id(TabTable.tabCOLUMN) + " = L." + id(TabTable.tabCOLUMN) + " AND " + id(TabTable.tabLEVEL) + " = " + m.mde().refToOtherTable();
    try (
        Statement stmt = getMetaConnection().createStatement();
        ResultSet res = stmt.executeQuery(query)
    ) {
      res.first();
      final String name = res.getString(DBmanager.name(TabTable.tabLEVEL));
      return new Ngram(name, Type.ATTR, new Entity(m.mde().refToOtherTable(), name, res.getInt(id(TabTable.tabTABLE)), Utils.getDataType(res.getString(type(TabTable.tabLEVEL))), TabTable.tabLEVEL), null);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * @param id id of the language predicate
   * @return the <type, name> of the language predicate
   */
  public static Pair<String, String> getPredicate(final int id) {
    final String query = "SELECT " + type(TabTable.tabLANGUAGEPREDICATE) + ", " + name(TabTable.tabLANGUAGEPREDICATE) + " FROM `" + TabTable.tabLANGUAGEPREDICATE + "` WHERE " + id(TabTable.tabLANGUAGEPREDICATE) + " = " + id;
    try (
        Statement stmt = getMetaConnection().createStatement();
        ResultSet res = stmt.executeQuery(query)
    ) {
      while (res.next()) {
        return Pair.of(res.getString(type(TabTable.tabLANGUAGEPREDICATE)), res.getString(name(TabTable.tabLANGUAGEPREDICATE)));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * @return a map <measure, operators> (i.e., the operators that are appliable to the given measure)
   */
  public static Map<String, Set<Entity>> getOperatorOfMeasure() {
    final Map<String, Set<Entity>> map = Maps.newLinkedHashMap();
    final String query = "select gm." + id(TabTable.tabGROUPBYOPERATOR) + ", gm." + id(TabTable.tabMEASURE) + ", " + name(TabTable.tabMEASURE) + ", " + name(TabTable.tabGROUPBYOPERATOR) + " " 
        + "from " + TabTable.tabGRBYOPMEASURE + " gm, " + TabTable.tabMEASURE + " m, " + TabTable.tabGROUPBYOPERATOR + " g "
        + "where g." + id(TabTable.tabGROUPBYOPERATOR) + " = gm." + id(TabTable.tabGROUPBYOPERATOR) + " and gm." + id(TabTable.tabMEASURE) + " = m." + id(TabTable.tabMEASURE) + "";
    try (
        Statement stmt = getMetaConnection().createStatement();
        ResultSet res = stmt.executeQuery(query)
    ) {
      while (res.next()) { // for each group by operator
        final String mea = res.getString(name(TabTable.tabMEASURE));
        final int id = res.getInt(id(TabTable.tabGROUPBYOPERATOR));
        final String op = res.getString(name(TabTable.tabGROUPBYOPERATOR));
        final Set<Entity> val = map.getOrDefault(mea, Sets.newLinkedHashSet());
        val.add(new Entity(id, op, TabTable.tabGROUPBYOPERATOR));
        map.put(mea, val);
      }
    } catch (final SQLException e) {
      e.printStackTrace();
    }
    return map;
  }

  /**
   * Save a query in the query dataset.
   * @param query nl query
   * @param gbset correct group by set
   * @param predicate correct selection clause
   * @param selclause correct measure clause
   */
  public static void saveQuery(final String query, final String gbset, final String predicate, final String selclause) {
      final String sql = "INSERT INTO `" + TabTable.tabQuery + "` (`" + TabTable.colQueryText + "`, `" + TabTable.colQueryGBset + "`, `" + TabTable.colQueryMeasClause + "`, `" + TabTable.colQuerySelClause + "`) "
                            + "VALUES (\"" + query + "\", \"" + gbset + "\", \"" + selclause + "\", \"" + predicate + "\")";
      executeQuery(sql);
  }

  public static Pair<String, String> getTabDetails(int idFT, int idTable) {
    final String query = "SELECT * FROM `" + TabTable.tabTABLE + "` WHERE `" + id(TabTable.tabTABLE) + "` = " + idTable;
    final String query1 = "SELECT * FROM `" + TabTable.tabCOLUMN + "` C INNER JOIN `" + TabTable.tabRELATIONSHIP + "` R ON C." + id(TabTable.tabRELATIONSHIP) + " = R." + id(TabTable.tabRELATIONSHIP) + " WHERE `" + TabTable.colRELTAB1 + "` = " + idFT + " AND `" + TabTable.colRELTAB2 + "` = " + idTable;
    try (
        Statement stmt = getMetaConnection().createStatement();
        ResultSet resDet = stmt.executeQuery(query);
        Statement stmt1 = getMetaConnection().createStatement();
        ResultSet resCol = stmt1.executeQuery(query1)
    ) {
      resDet.first();
      resCol.first();
      return Pair.of(resDet.getString(name(TabTable.tabTABLE)), resCol.getString(name(TabTable.tabCOLUMN)));
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Returns the member of each level.
   * @return set members (i.e., entities) for each levels
   */
  public static Map<String, Set<Entity>> getMembersOfLevels() {
    final String query = "select m." + id(TabTable.tabMEMBER) + ", " + name(TabTable.tabMEMBER) + ", m." + id(TabTable.tabLEVEL) + ", " + type(TabTable.tabLEVEL) + ", " + name(TabTable.tabLEVEL) + " from level l, member m where l." + id(TabTable.tabLEVEL) + " = m." + id(TabTable.tabLEVEL);
    try (
        Statement stmt = getMetaConnection().createStatement();
        ResultSet res = stmt.executeQuery(query);
    ) {
      final Map<String, Set<Entity>> members = Maps.newLinkedHashMap();
      while (res.next()) {
        final Set<Entity> tmp = members.getOrDefault(res.getString(name(TabTable.tabLEVEL)), Sets.newLinkedHashSet());
        tmp.add(new Entity(res.getInt(id(TabTable.tabMEMBER)), res.getString(name(TabTable.tabMEMBER)), res.getInt(id(TabTable.tabLEVEL)), Utils.getDataType(res.getString(type(TabTable.tabLEVEL))), TabTable.tabMEMBER));
        members.put(res.getString(name(TabTable.tabLEVEL)), tmp);
      }
      return members;
    } catch (final SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Returns the member of each level.
   * @return set members (i.e., entities) for each levels
   */
  public static Map<String, Set<Entity>> getLevelsOfMembers() {
    final String query = "select * from member m, `level` a, `column` c, `table` t where c.table_id = t.table_id and a.column_id = c.column_id and m.level_id = a.level_id";
    try (
        Statement stmt = getMetaConnection().createStatement();
        ResultSet res = stmt.executeQuery(query);
    ) {
      final Map<String, Set<Entity>> attributes = Maps.newLinkedHashMap();
      while (res.next()) {
        final Set<Entity> tmp = attributes.getOrDefault(res.getString(name(TabTable.tabMEMBER)), Sets.newLinkedHashSet());
        tmp.add(new Entity(res.getInt(id(TabTable.tabLEVEL)), res.getString(name(TabTable.tabLEVEL)), res.getInt(id(TabTable.tabTABLE)), Utils.getDataType(res.getString(type(TabTable.tabLEVEL))), TabTable.tabLEVEL));
        attributes.put(res.getString(name(TabTable.tabMEMBER)), tmp);
      }
      return attributes;
    } catch (final SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Get levels with type "year". TODO: in this version only the level 'the_year' is retrieved. This is because it not easy to find "year" levels.
   * @return levels with type "year"
   */
  public static Set<Entity> getYearLevels() {
    final String query = "select * from `level` a, `column` c, `table` t where c.table_id = t.table_id and a.column_id = c.column_id and level_name = 'the_year'";
    try (
        Statement stmt = getMetaConnection().createStatement();
        ResultSet res = stmt.executeQuery(query);
    ) {
      final Set<Entity> attributes = Sets.newLinkedHashSet();
      while (res.next()) {
        attributes.add(new Entity(res.getInt(id(TabTable.tabLEVEL)), res.getString(name(TabTable.tabLEVEL)), res.getInt(id(TabTable.tabTABLE)), Utils.getDataType(res.getString(type(TabTable.tabLEVEL))), TabTable.tabLEVEL));
      }
      return attributes;
    } catch (final SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static Entity getLevel(String string) {
    final String query = "select * from `level` a, `column` c, `table` t where c.table_id = t.table_id and a.column_id = c.column_id and level_name = \"" + string.trim() + "\"";
    try (
        Statement stmt = getMetaConnection().createStatement();
        ResultSet res = stmt.executeQuery(query);
    ) {
      final List<Entity> attributes = Lists.newArrayList();
      while (res.next()) {
        attributes.add(new Entity(res.getInt(id(TabTable.tabLEVEL)), res.getString(name(TabTable.tabLEVEL)), res.getInt(id(TabTable.tabTABLE)), Utils.getDataType(res.getString(type(TabTable.tabLEVEL))), res.getString(name(TabTable.tabTABLE))));
      }
      return attributes.get(0);
    } catch (final Exception e) {
      System.out.println(string);
      e.printStackTrace();
    }
    return null;
  }
}
