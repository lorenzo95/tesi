package it.unibo.conversational.database.graphic.help;

import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;

import it.unibo.conversational.database.listenerPanel.ModelInsertListener;

public class GraphicLoad {
	private static JComboBox<String> comboBox = new JComboBox<>();	
	private static String fileQuerySave = "ListQuery.txt";
	
	public static JComboBox<String> getComboBox() {
		return comboBox;
	}
	
	private static void fillListIntentions() {
		List<String> listIntentions = new ArrayList<>();
		try {
			BufferedReader out = new BufferedReader(new FileReader(fileQuerySave));
			String ret;
			while((ret = out.readLine()) != null) {
				listIntentions.add(ret);
			}
			out.close();
		} catch (IOException exc) {
			System.out.println("Eccezione = ".concat(exc.getMessage()));
		}
		
		String[] itemBox = new String[listIntentions.size() + 1];
		itemBox[0] = "";
		for (int i = 0; i < listIntentions.size(); i++) {
			itemBox[i+1] = listIntentions.get(i);
		}
		
		for (int i = 0; i < itemBox.length; i++) {
			comboBox.addItem(itemBox[i]);
		}
	}
	
	public static void makeListIntenions() {
		GraphicApp.getLoadPanel().add(new JLabel("        Request List:"));
		comboBox.addActionListener(new ModelInsertListener());
		comboBox.setPreferredSize(new Dimension(700, 25));
		GraphicApp.getLoadPanel().add(comboBox);
		fillListIntentions();
	}
}