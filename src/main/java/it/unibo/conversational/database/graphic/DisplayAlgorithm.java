package it.unibo.conversational.database.graphic;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;

import it.unibo.conversational.database.listener.BtnAlghoritmListener;
import it.unibo.conversational.database.listener.saveLoad.ButtonSave;
import it.unibo.conversational.database.myAlgorithms.PythonKmeans;

public class DisplayAlgorithm {
	private static final String currentPath = new java.io.File("").getAbsolutePath();
	private static final String pathImmagini = "/src/main/resources/immagini/";
	private static final String EXTENSION_IMAGE = ".gif";
	
	private static void setImagePanel(JPanel commandPanel, final String name) {
		for (int i = 0; i < 3; i++) {
			try {
				JLabel labelImage = new JLabel();
				ImageIcon img = new ImageIcon(currentPath.concat(pathImmagini).concat(name+i).concat(EXTENSION_IMAGE));
				labelImage.setIcon(img); 
				labelImage.setPreferredSize(new Dimension(30, 20));
				commandPanel.add(labelImage);
			} catch (Exception exc) {}
		}
	}
	
	public static void displayAlghoritm(JPanel commandPanel, JTable jtable, List<String> labelAlgorithms) {
		commandPanel.setLayout(new GridLayout(2, 1)); // prova
		
		// Button Panel
		JPanel buttonPanel = new JPanel(new GridLayout(labelAlgorithms.size()+5, 1));
		buttonPanel.add(new JLabel("Algorithms:            "));
		buttonPanel.add(new JLabel("     ")); // Dummy
		
		BtnAlghoritmListener btnListener = new BtnAlghoritmListener(jtable);
		labelAlgorithms.forEach(alg -> {
			JButton btnAlg = new JButton(alg);
			btnAlg.setPreferredSize(new Dimension(7, 5));
			btnAlg.addActionListener(btnListener);
			buttonPanel.add(btnAlg);
			buttonPanel.add(Box.createVerticalGlue());
		});
		
		// DashBoard Panel (da separare)
		buttonPanel.add(new JLabel("DashBoard:         "));
		buttonPanel.add(new JLabel("     ")); // Dummy
		JButton btnSaveDash = new JButton("Save");
		JButton btnLoadDash = new JButton("Load");

		btnSaveDash.addActionListener(new ButtonSave());
		btnLoadDash.addActionListener(new ButtonSave());  // ?? da modificare

		buttonPanel.add(btnSaveDash);
		buttonPanel.add(new JLabel("      ")); // Dummy
		buttonPanel.add(btnLoadDash);
		commandPanel.add(buttonPanel);
		
		// Image Panel
		JPanel imagePanel = new JPanel();
		imagePanel.setLayout(new GridLayout(0, 2,10,10));

		imagePanel.add(new JLabel("Graphics: "));
//		imagePanel.add(new JLabel("              "));
		labelAlgorithms.forEach(alg -> {setImagePanel(imagePanel, alg);});
		
		commandPanel.add(imagePanel);
		}
	
	public static List<Object> computationKMeans(int numCluster) {
		List<String> kMeans = PythonKmeans.getKmeans(numCluster);
		List<String> listData = TempGraphic.getDataLabel();
		List<Object> listBelongCluster = new ArrayList<>();
		int i, j, dimKmeans = kMeans.size(), dimListData = listData.size();
		
		for (j = 0; j < dimListData; j++) {
			String nameStrore = listData.get(j); // per ciascun NameStore
			boolean found = false;
			for (i = 0; i < dimKmeans && !found; i++) { // trovo la parola su ciascun cluster
				String nameCluster = kMeans.get(i);
				String[] nameClusterArray = nameCluster.split(";"); 
				
				for (int f = 0; f < nameClusterArray.length && !found; f++) { // per ogni dato nel cluster
					String nmClsClean = nameClusterArray[f].trim();
					if (nmClsClean.equals(nameStrore)) { // dato trovato nel cluster
						listBelongCluster.add(nameStrore.concat(" in") + (i+1)); found = true; // un dato appartiene esattamente a 1 cluster
					}
				}
			}
		}
		return listBelongCluster;
	}
}