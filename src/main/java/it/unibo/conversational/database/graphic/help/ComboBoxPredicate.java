package it.unibo.conversational.database.graphic.help;

import javax.swing.JComboBox;

public class ComboBoxPredicate<E> extends JComboBox<E> {
	private static final long serialVersionUID = 904586354;

	private String identifier;
	
	public void setIdentifier(final String identifier) { this.identifier = identifier; }
	public String getIdentifier() { return identifier; }
	
	public ComboBoxPredicate(final E itemLevel[]) {
		super(itemLevel);
	}
}
