package it.unibo.conversational.database.graphic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.darkprograms.speech.microphone.Microphone;
import com.darkprograms.speech.recognizer.GSpeechDuplex;
import com.darkprograms.speech.recognizer.GSpeechResponseListener;
import com.darkprograms.speech.recognizer.GoogleResponse;

import it.unibo.conversational.database.graphic.help.GraphicApp;
import it.unibo.conversational.database.listener.ClearButtonListener;
import it.unibo.conversational.database.listener.SubmitButtonListener;
import it.unibo.conversational.database.speech.Corrector;
import it.unibo.conversational.database.speech.SpeechListener;
import net.sourceforge.javaflacencoder.FLACFileWriter;

public class MainApp implements GSpeechResponseListener {
	public static final Microphone mic = new Microphone(FLACFileWriter.FLAC);
	public static GSpeechDuplex duplex = new GSpeechDuplex("AIzaSyBOti4mM-6x9WDnZIjIeyEU21OpBXqWBgw");

	public static void listenerRecord() {		
		duplex.setLanguage("en");
		
		final JButton record = GraphicApp.getRecord();
		final JButton stop = GraphicApp.getStop();
		record.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				new Thread(() -> {
					try {
						duplex.recognize(mic.getTargetDataLine(), mic.getAudioFormat());
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}).start();
				record.setEnabled(false);
				stop.setEnabled(true);
			}
		});
		stop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				mic.close();
				duplex.stopSpeechRecognition();
				record.setEnabled(true);
				stop.setEnabled(false);
				
				GraphicApp.getResponse().setText(Corrector.correctInput(GraphicApp.getResponse().getText()));
			}
		});

		GraphicApp.getSubmitButton().addActionListener(new SubmitButtonListener(GraphicApp.getResponse(), GraphicApp.getCommandPanel(), GraphicApp.getMsgArea(), GraphicApp.getJTable()));
		GraphicApp.getClearButton().addActionListener(new ClearButtonListener(GraphicApp.getCommandPanel(), GraphicApp.getMsgArea(), GraphicApp.getJTable()));
		duplex.addResponseListener(new SpeechListener(GraphicApp.getResponse()));
	}
	
	@Override
	public void onResponse(GoogleResponse paramGoogleResponse) {}
}