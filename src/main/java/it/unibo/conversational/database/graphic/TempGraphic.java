package it.unibo.conversational.database.graphic;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

import it.unibo.conversational.database.graphic.help.GraphicApp;
import it.unibo.conversational.database.help.Utils;

public class TempGraphic {
	private static List<Map<String, List<Object>>> listMap = new ArrayList<>();
	private static List<String> dataLabel = new ArrayList<>();
	private static int numCol;
	
	private static int numCluster = 3;
	public static void setNumCluster(int num) { numCluster = num; }
	public static int getNumCluster() { return numCluster; }
	
	private static String selectedItemReminder = "selectedItemReminder";
	
	public static String getString(ActionEvent e) {
		String itemSelect = String.valueOf(e.getSource());
		
		int lenItem = selectedItemReminder.length();
		int indexOfItem = itemSelect.indexOf(selectedItemReminder) + 1;
		int lastSquare = itemSelect.lastIndexOf("]");
		
		int indexItem = (indexOfItem + lenItem);
		
		String itemSelected = itemSelect.substring(indexItem, lastSquare);
		return itemSelected;
	}
	
	public static void cleanGraphic(JPanel commandPanel, JTextArea msgArea, JTable jtable) {
	  commandPanel.removeAll();
	  msgArea.setText("");
	  GraphicApp.outValue.setText("");

	  DefaultTableModel modelD = (DefaultTableModel) jtable.getModel();
	  
	  for(int i = jtable.getRowCount() - 1; i >= 0; i--){ modelD.removeRow(i); }
	  for (int i = 0; i < modelD.getColumnCount(); i++) {
		  jtable.getColumnModel().getColumn(i).setHeaderValue("");
	  }

	  listMap = new ArrayList<>();
	  dataLabel = new ArrayList<>();
	  numCol = 0;
	  
	  jtable.getParent().getParent().repaint();
	}
	
	public static List<Map<String, List<Object>>> getListMap() { return listMap; }
	public static List<String> getDataLabel() { return dataLabel; }
	
	public static void showData(String fileParsing, JTable jtable) {
		Map<String, List<Object>> map = Utils.readCsvDouble(fileParsing);
		
		if (!map.isEmpty()) {
			listMap.add(map);
			Set<String> setKey = map.keySet();
			int dimKeySize = setKey.size();
			DefaultTableModel modelD = (DefaultTableModel) jtable.getModel();
			
			for (String keyFor : setKey) {		
				List<Object> listDouble = map.get(keyFor);
				if (dimKeySize > 1 && numCol == (dimKeySize-1)) {
					listDouble.stream().map(el -> String.valueOf(el)).forEach(str -> {
						dataLabel.add(str);
					});
				}
							
				int diff = listDouble.size() - modelD.getRowCount();
				for (int i = 0; i < diff; i++) { modelD.addRow(new Object[] {"", "", "", "", ""}); }
				jtable.getColumnModel().getColumn(numCol).setHeaderValue(keyFor);
				
				for (int i = 0; i < listDouble.size(); i++) { 
					modelD.setValueAt(listDouble.get(i), i, numCol);
				}
				numCol++;
			}
		}
	}
}