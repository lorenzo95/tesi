package it.unibo.conversational.database.graphic.help;

import java.awt.Dimension;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import it.unibo.conversational.database.help.Enum.NumDatabase;
import it.unibo.conversational.database.help.Enum.TokenOperator;
import it.unibo.conversational.database.help.QueryDb;
import it.unibo.conversational.database.help.parsing.HelperMeasure;
import it.unibo.conversational.database.help.parsing.HelperPredicate;
import it.unibo.conversational.database.listenerPanel.ComboBoxListener;
import it.unibo.conversational.database.listenerPanel.MeasureComboBoxListener;
import it.unibo.conversational.database.parsing.OperationHelper;

public class GraphicAux {
	private static JComboBox<String> jcomboMeasure;
	private static ComboBoxPredicate<String> jcomboPredicate;
	private static ComboBoxPredicate<String> jcomboLevel;
	
	public static JComboBox<String> getJcomboMeasure() { return jcomboMeasure; }
	public static ComboBoxPredicate<String> getJcomboPredicate() { return jcomboPredicate; }
	public static ComboBoxPredicate<String> getJcomboLevel() { return jcomboLevel; }

	public static void resetMeasurePredicate() {
		jcomboMeasure.removeAllItems(); jcomboPredicate.removeAllItems();
		jcomboLevel.removeAllItems();
		
		final String[] measures = getMeasure();
		for (int i = 0; i < measures.length; i++) { jcomboMeasure.addItem(measures[i]); }
		final String[] predicates = getItemLevel();
		for (int i = 0; i < predicates.length; i++) { 
			jcomboPredicate.addItem(predicates[i]);
			jcomboLevel.addItem(predicates[i]);
		}
	}
	
	private static String[] getMeasure() {
		HelperMeasure.setMeasureDatabase(QueryDb.getData(OperationHelper.getConnectionMetaData(), "measure_name", "measure"));
		List<String> listMeasure = HelperMeasure.getMeasureDatabase();
		String[] itemMeasure = new String[listMeasure.size() + 2];
		
		if (listMeasure.isEmpty()) { GraphicApp.msgArea.append("Query to conversional not succesfull"); }	
		itemMeasure[0] = ""; itemMeasure[1] = "<measure>";
		for (int i = 0; i < listMeasure.size(); i++) { itemMeasure[i+2] = listMeasure.get(i); }
		return itemMeasure;
	}
	
	private static String[] getItemLevel() {
		HelperPredicate.setLevel(QueryDb.getData(OperationHelper.getConnectionMetaData(), "level_name", "level"));
		List<String> listLevel = HelperPredicate.getLevel();
		String[] itemLevel = new String[listLevel.size() + 2];
		
		if (listLevel.isEmpty()) { GraphicApp.msgArea.append("Query to conversional not succesfull"); }	
		itemLevel[0] = ""; itemLevel[1] = "<predicate>";
		for (int i = 0; i < listLevel.size(); i++) { itemLevel[i+2] = listLevel.get(i); }
		return itemLevel;
	}
	
	private static void makeComboMeasure(JPanel panelOperation) {		
		jcomboMeasure = new JComboBox<>(getMeasure());
		jcomboMeasure.setPreferredSize(new Dimension(100, 25));
		jcomboMeasure.addActionListener(new MeasureComboBoxListener());
		panelOperation.add(jcomboMeasure);
	}
		
	private static void makeComboLevel(JPanel panelOperation, final String identifier) {		
		ComboBoxPredicate<String> jcomboBox;
		if (identifier.equals("predicate")) {
			jcomboPredicate = new ComboBoxPredicate<>(getItemLevel());
			jcomboBox = jcomboPredicate;
		} else {
			jcomboLevel = new ComboBoxPredicate<>(getItemLevel());
			jcomboBox = jcomboLevel;
		}
		jcomboBox.setPreferredSize(new Dimension(100, 25));
		jcomboBox.setIdentifier(identifier);
		jcomboBox.addActionListener(new ComboBoxListener());
		panelOperation.add(jcomboBox);
	}
	
	private static void createItemPanel(JPanel panelTemp2, String OpOrCube, String firstElem, String nameMart, String nameConv) {
		panelTemp2.add(new JLabel(OpOrCube));
		String[] itemCube = new String[] {"", firstElem, nameMart, nameConv};
		JComboBox<String> jcomboBoxCube = new JComboBox<>(itemCube);
		jcomboBoxCube.addActionListener(new MeasureComboBoxListener());
		panelTemp2.add(jcomboBoxCube);
	}
	
	public static void makePanelOperation(JPanel panelTemp2) {
		createItemPanel(panelTemp2, " Cube:", "<cube>", NumDatabase.FOOD_MART.getName(), NumDatabase.COVID_MART.getName());
		createItemPanel(panelTemp2, "  Operator:", "<operator>", TokenOperator.DESCRIBE.getName(), TokenOperator.EXPLAIN.getName());
		
		panelTemp2.add(new JLabel("  Measures:"));
		GraphicAux.makeComboMeasure(panelTemp2);
				
		panelTemp2.add(new JLabel("  Predicate:"));
		GraphicAux.makeComboLevel(panelTemp2, "predicate");
		panelTemp2.add(new JLabel("  Levels:"));
		GraphicAux.makeComboLevel(panelTemp2, "level");
	}
}