package it.unibo.conversational.database.graphic.help;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import it.unibo.conversational.database.listenerPanel.ModelInsertListener;

public class GraphicApp {
	private static JFrame frame = new JFrame("IAM Prototype");
	private static JPanel loadPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	
	private static final JButton submitButton = new JButton("Submit");
	private static final JButton clearButton = new JButton("Clear");
	private static final JButton record = new JButton("Record");
	private static final JButton stop = new JButton("Stop");
		
	private static final JTable jtable = new JTable(new DefaultTableModel(0, 5)); // tabella dati
	private static final JPanel commandPanel = new JPanel();
	
	private static JTextArea response = new JTextArea(2, 70);
	public static final JTextArea outValue = new JTextArea(5, 5);
	public static final JTextArea msgArea = new JTextArea(4,60);
	
	public static JPanel getLoadPanel() { return loadPanel; }
	
	public static JButton getRecord() { return record; }
	public static JButton getStop() { return stop; }
	public static JButton getSubmitButton() { return submitButton; }
	public static JButton getClearButton() { return clearButton; }
	
	public static JTextArea getResponse() { return response; }
	public static JTable getJTable() { return jtable; }
	public static JPanel getCommandPanel() { return commandPanel; }
	public static JTextArea getMsgArea() { return msgArea; }
	
	private static String fileModelSave = "ListModels.txt";
	
	private static String[] getItemModel() {
		try {
			BufferedReader out = new BufferedReader(new FileReader(fileModelSave));
			String ret = "";
			
			List<String> listReadLines = new ArrayList<>();
			while((ret = out.readLine()) != null) { listReadLines.add(ret); }
			out.close();
			String[] stringItemModel = new String[listReadLines.size()];
			for (int i = 0; i < listReadLines.size(); i++) {
				stringItemModel[i] = listReadLines.get(i);
			}
			return stringItemModel;
		} catch (IOException exc) {
			System.out.println("Eccezione = ".concat(exc.getMessage())); return new String[] {};
		}
	}
	
	public static void makeRecordBar() {
		JPanel contenitor = new JPanel();
		contenitor.setLayout(new BoxLayout(contenitor, BoxLayout.PAGE_AXIS));
		contenitor.add(new JLabel("Intentional request: | describe | explain |", SwingConstants.LEFT));
		
		JPanel modelPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		modelPanel.add(new JLabel("          Models List:"));		
		JComboBox<String> comboBox = new JComboBox<>(getItemModel());
		comboBox.addActionListener(new ModelInsertListener());
		modelPanel.add(comboBox);
		contenitor.add(modelPanel);
		
		GraphicLoad.makeListIntenions(); 
		contenitor.add(loadPanel);
		
		JPanel responsePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		responsePanel.add(new JLabel("Intention request:"));
		JScrollPane scroll = new JScrollPane(response);
		responsePanel.add(scroll);
		contenitor.add(responsePanel);
		
		JPanel panelOperation = new JPanel();
		panelOperation.setLayout(new BoxLayout(panelOperation, BoxLayout.PAGE_AXIS));
		
		JPanel panelConv = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panelConv.add(new JLabel("Metadata: "));
		GraphicAux.makePanelOperation(panelConv);
		panelOperation.add(panelConv);
		
		JPanel panelButton = new JPanel();
		panelButton.add(submitButton);
		panelButton.add(clearButton);
		panelButton.add(record);
		panelButton.add(stop);
		panelOperation.add(panelButton);
		contenitor.add(panelOperation);
		
		frame.getContentPane().add(contenitor, BorderLayout.NORTH);
	}
	
	public static void makeTable() {		
		JPanel outPanel = new JPanel();
		JScrollPane scrollPanel2 = new JScrollPane(jtable);
		outPanel.setLayout(new FlowLayout());
		
		outPanel.add(scrollPanel2);
		outPanel.add(commandPanel);
		frame.getContentPane().add(outPanel, BorderLayout.CENTER);
	}
	
	public static void makeMsgPanel() {
		JPanel msgPanel = new JPanel();
		msgPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		msgArea.setAutoscrolls(true);
		msgPanel.add(msgArea);
		frame.getContentPane().add(msgPanel, BorderLayout.SOUTH);
	}
	
	public static void defaultOpFrame() {
		frame.setDefaultCloseOperation(3);
		response.setEditable(true);
		response.setWrapStyleWord(true);
		response.setLineWrap(true);
		stop.setEnabled(false);
	}	

	public static void makeFrameVisible() {
		frame.setVisible(true);
		frame.pack();
		frame.setSize(1250, 750);
		frame.setLocationRelativeTo(null);
	}
}