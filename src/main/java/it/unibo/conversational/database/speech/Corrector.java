package it.unibo.conversational.database.speech;

import java.util.Scanner;

import it.unibo.conversational.Utils;
import it.unibo.conversational.database.help.Enum.NumDatabase;
import it.unibo.conversational.database.help.Enum.TokenOperator;
/*
import it.unibo.conversational.database.help.Enum.TokenOperator;
import it.unibo.conversational.database.help.parsing.HelperMeasure;
import it.unibo.conversational.database.help.parsing.HelperPredicate;
*/
import it.unibo.conversational.database.parsing.OperationHelper;

public class Corrector {
	private static int numOP = 0;
	
	private static void evaluateCube(String cube) {
		OperationHelper.setConnectionData(cube);
		String conversional = (cube.equals(NumDatabase.FOOD_MART.getName())) ? 
				NumDatabase.CONVERSATIONAL.getName() : NumDatabase.COVID_METADATA.getName();
		OperationHelper.setConnectionMetaData(conversional);
	}
	
	private static void goForward(Scanner scanner) {
		for (int i = 0; i < numOP; i++) {
			if (scanner.hasNext()) {
				scanner.next();
			}
		}
		numOP++;
	}
	
	private static String subWith(String input) {
		Scanner scanner = new Scanner(input); goForward(scanner);
		if (scanner.hasNext()) {
			input = input.replace(scanner.next(), TokenOperator.WITH.getName());
			scanner.close();
		}
		scanner.close();
		return input;
	}
	
	private static String valuteDatabase(String input) {
		Scanner scanner = new Scanner(input); goForward(scanner);
		if (scanner.hasNext()) {
			String opString = scanner.next();			
			double firstDistance = Utils.tokenSimilarity(NumDatabase.FOOD_MART.getName(), opString);
			double secondDistance = Utils.tokenSimilarity(NumDatabase.COVID_MART.getName(), opString);
			
			if (firstDistance > secondDistance) {
				input = input.replace(opString, NumDatabase.FOOD_MART.getName());
				evaluateCube(NumDatabase.FOOD_MART.getName());
			} else {
				input = input.replace(opString, NumDatabase.COVID_MART.getName());
				evaluateCube(NumDatabase.COVID_MART.getName());
			}
		}
		scanner.close();
		return input;
	}
	
	private static String valuteDistanceOperator(String input) {
		Scanner scanner = new Scanner(input); goForward(scanner);
		if (scanner.hasNext()) {
			String opString = scanner.next();
			double firstDistance = Utils.tokenSimilarity(TokenOperator.DESCRIBE.getName(), opString);
			double secondDistance = Utils.tokenSimilarity(TokenOperator.EXPLAIN.getName(), opString);
			
			if (firstDistance > secondDistance) {
				input = input.replace(opString, TokenOperator.DESCRIBE.getName());
			} else {
				input = input.replace(opString, TokenOperator.EXPLAIN.getName());
			}
		}
		scanner.close();
		return input;
	}
	
	public static String correctInput(String input) {
		input = subWith(input);
		input = valuteDatabase(input);
		input = valuteDistanceOperator(input);
		numOP = 0;
		
		return input;
	}
}