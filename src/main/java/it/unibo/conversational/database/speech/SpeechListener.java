package it.unibo.conversational.database.speech;

import javax.swing.JTextArea;

import com.darkprograms.speech.recognizer.GSpeechResponseListener;
import com.darkprograms.speech.recognizer.GoogleResponse;

public class SpeechListener implements GSpeechResponseListener {
	private String old_text = "";
	public static JTextArea responseListener;
	
	public SpeechListener(JTextArea response) { responseListener = response; }
	
	@Override
	public void onResponse(GoogleResponse gr) {
		String output = gr.getResponse();				

		if (output == null) {
			this.old_text = responseListener.getText();
			if (this.old_text.contains("(")) {
				this.old_text = this.old_text.substring(0, this.old_text.indexOf('('));
			}
			System.out.println("Paragraph Line Added");
			this.old_text = (responseListener.getText().concat("\n"));
			this.old_text = this.old_text.replace(")", "").replace("( ", "");
			responseListener.setText(this.old_text);
			return;
		}
		if (output.contains("(")) {
			output = output.substring(0, output.indexOf('('));
		}
		if (!gr.getOtherPossibleResponses().isEmpty()) {
			output = output.concat(" (").concat(gr.getOtherPossibleResponses().get(0).concat(")"));
		}
		System.out.println(output);
		responseListener.setText("");
		responseListener.append(this.old_text);		
		responseListener.append(output);
	}
}
