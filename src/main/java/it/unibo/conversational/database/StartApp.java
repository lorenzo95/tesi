package it.unibo.conversational.database;

import javax.swing.SwingUtilities;

import it.unibo.conversational.database.graphic.MainApp;
import it.unibo.conversational.database.graphic.help.GraphicApp;

//"Describe: with C describe m1,m2,m3,m4 [for P] by l [size int>0]"
//"Explain: with C explain m1,m2,m3,m4 [for SubC] using t1,t2,t3 by n"
public class StartApp {
//	with foodmart describe sum(store_cost), maximum(unit_sales) for the_year >= 1997 by store_name
//  with foodmart describe sum(store_cost), maximum(unit_sales) for the_year >= 1997 by store_name size 4
	
//	with foodmart explain store_sales for the_year=1997 using next(year) by store_name
//	with foodmart explain store_sales for the_year=1997 using correlation(unit_sales) by store_name
//	with foodmart explain store_sales for the_month=January using correlation(unit_sales) by store_name
//	with foodmart explain store_sales for the_month=January using next(month) by store_name
//	with foodmart explain store_sales for the_month=March using past(month) by store_name
//	with foodmart explain store_sales for promotion_name=Wallet Savers using correlation(unit_sales) by store_name
//  with foodmart explain store_sales for the_year=1997 and promotion_name=Wallet Savers using correlation(unit_sales) by store_name
	
	public static void main(String[] args) {
		GraphicApp.makeRecordBar();
		GraphicApp.makeTable();
		GraphicApp.makeMsgPanel();
		MainApp.listenerRecord();
		GraphicApp.defaultOpFrame();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				GraphicApp.makeFrameVisible();
			}
		});
	}
}