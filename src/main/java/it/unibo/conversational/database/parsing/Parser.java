package it.unibo.conversational.database.parsing;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import it.unibo.conversational.database.DBmanager;
import it.unibo.conversational.database.graphic.TempGraphic;
import it.unibo.conversational.database.help.Enum.NumDatabase;
import it.unibo.conversational.database.help.Enum.TokenOperator;
import it.unibo.conversational.database.help.parsing.HelperBase;
import it.unibo.conversational.database.help.parsing.HelperMeasure;

public class Parser {	
	private static Connection conn;
	
	private static String SIZE = "size";
	
	public static String jsonmeasures = "";
	public static String jsonpredicate = "";
	public static String jsongroupby;
	
	private static int operator_index;
	private static int last_index;
	
	public static int getOperator_index() { return operator_index; }
	public static void setOperator_index(int operator_index) { 
		Parser.operator_index = operator_index;
	}
	public static int getLast_index() { return last_index; }
	public static void setLast_index(int last_index) {
		Parser.last_index = last_index;
	}

	private static String error = "";
	public static String getError() { return error; }
	public static void setError(String er) { error = er; }
	
	protected static void setMetadataConnection(final String input) {
		if (input.equals(NumDatabase.FOOD_MART.getName())) {
			conn = DBmanager.getConnection(NumDatabase.CONVERSATIONAL.getName());
		} else if (input.equals(NumDatabase.COVID_MART.getName())) {
			conn = DBmanager.getConnection(NumDatabase.COVID_METADATA.getName());
		}
	}
	
	public static int operationParsing(String input, Scanner scanner, String INDEX, Connection conn) {
		operator_index = input.indexOf(INDEX);
		last_index = -1;
				
		int by_using_index = -1;
		if (INDEX.equals(TokenOperator.EXPLAIN.getName())) {
			by_using_index = input.indexOf(TokenOperator.USING.getName());
		} else {
			by_using_index = input.indexOf(TokenOperator.BY.getName());
		}
				
		int for_index = input.indexOf(TokenOperator.FOR.getName());
		if (by_using_index <= operator_index || (for_index != -1 && by_using_index <= for_index)) {
			if (getError().isEmpty()) {
				setError("For Index not properly set"); 
			}	return -1;
		}
		if (for_index > 0) { last_index = for_index; } 
		else { last_index = by_using_index; }
				
		jsonmeasures = ",\"MC\":[";
		String measure = HelperMeasure.parseMeasure(input, operator_index, last_index, INDEX, conn);
		if (!measure.isEmpty()) {
			jsonmeasures = jsonmeasures.concat(measure);
			last_index = by_using_index;
			jsonpredicate = "\"SC\":[";
			
			String pred = AuxParser.setForIndex(input, for_index, conn);
			if (!pred.isEmpty()) { jsonpredicate = jsonpredicate.concat(pred); }
			else {return -1;}
			jsongroupby = ", \"GC\":[";
			
			return by_using_index;
		}
		return -1;
	}
				
	public static List<String> parsing(String input, Scanner scanner) {
		Parser.setError(""); // reset errore all'inizio della query
		input = AuxParser.withFind(input, scanner); if (input.isEmpty()) { return new ArrayList<>(); }
		
		String exp = scanner.next();
		if (exp.equals(TokenOperator.EXPLAIN.getName())) {
			return HelperBase.findExplain(input, scanner, conn);
		} else if (exp.equals(TokenOperator.DESCRIBE.getName())) {
			if (operationParsing(input, scanner, TokenOperator.DESCRIBE.getName(), conn) < 0) {
				try {conn.close();} catch(SQLException e) {}; return new ArrayList<>();
			}
			
			int sizeIndex = input.indexOf(SIZE);
			if (sizeIndex > 0) {
				int len = SIZE.length();
				int beginIndex = sizeIndex + len + 1;
				int endIndex = beginIndex + 1;
				int numCluster = Integer.valueOf(input.substring(beginIndex, endIndex)).intValue();
				TempGraphic.setNumCluster(numCluster);
				
				input = input.substring(0, sizeIndex);
			}
			
			if (AuxParser.byIndex(input, conn) < 0) {
				try {conn.close();} catch(SQLException e) {}; return new ArrayList<>();
			}
		} else { 
			if (getError().isEmpty()) { error = "Explain or Describe index not properly set"; } 
			try {scanner.close(); conn.close();} catch(SQLException e) {}; 
			return new ArrayList<>(); 
		}
		
		return HelperBase.closeQuery();
		}
}