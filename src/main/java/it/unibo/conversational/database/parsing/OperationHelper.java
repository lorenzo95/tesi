package it.unibo.conversational.database.parsing;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.JPanel;
import javax.swing.JTable;

import org.json.JSONObject;
import com.opencsv.CSVWriter;

import it.unibo.conversational.database.DBmanager;
import it.unibo.conversational.database.graphic.DisplayAlgorithm;
import it.unibo.conversational.database.help.Enum.NameAlgorithms;
import it.unibo.conversational.database.help.Enum.NumDatabase;
import it.unibo.conversational.database.help.QueryDb;

public class OperationHelper {
	public static Connection connMetaData;
	private static Connection connData;
	
	private static String intention = ""; 
	public static void setIntention(final String intent) {
		if (intent != null && !intent.isEmpty()) { intention = intent; }
	}
	public static String getIntention() { return intention; }
	
	public static List<String> getQuery(JTable jtable, JPanel commandPanel, String parsingString) {
		List<String> parsingList = new ArrayList<>(), labelAlgorithms = new ArrayList<>();		
		labelAlgorithms.add(NameAlgorithms.OUTLIER.getName());
				
		if (parsingString.indexOf("explain") > 0) {
			if (parsingString.indexOf("correlation") > 0) {
				labelAlgorithms.add(NameAlgorithms.ANOVA.getName());
				labelAlgorithms.add(NameAlgorithms.PEARSON.getName());
			} else {
				labelAlgorithms.add(NameAlgorithms.TREND.getName());
			}
		} else if (parsingString.indexOf("describe") > 0) {
			labelAlgorithms.add(NameAlgorithms.TOP_K.getName());
			labelAlgorithms.add(NameAlgorithms.K_MEANS.getName());
		}
		DisplayAlgorithm.displayAlghoritm(commandPanel, jtable, labelAlgorithms);
		
		setIntention(parsingString);
		Scanner scanner = new Scanner(parsingString);
		parsingList = Parser.parsing(parsingString, scanner);
		
		List<String> queryArray = new ArrayList<>();
		parsingList.stream().filter(parse -> !parse.isEmpty()).forEach(elem -> {
			queryArray.add(QueryDb.createQuery(new JSONObject(elem)));
		});
		
		return queryArray;
	}
	
	public static void setConnectionData(final String nameDb) { connData = DBmanager.getConnection(nameDb); }
	public static void setConnectionMetaData(final String nameDb) { connMetaData = DBmanager.getConnection(nameDb); }
	
	public static Connection getConnectionData() {
		if (connData == null) {
			setConnectionData(NumDatabase.FOOD_MART.getName());
		}
		return connData;
	}
	
	public static Connection getConnectionMetaData() {
		if (connMetaData == null) {
			setConnectionMetaData(NumDatabase.CONVERSATIONAL.getName());
		}
		return connMetaData;
	}
	
	public static void writeFile(List<String> queries, String fileWrite) {
		try {
		Statement st = getConnectionData().createStatement();
		List<String> queryTransform = new ArrayList<>();
		queries.forEach(qry -> { 
			if (qry != null && !qry.isEmpty()) queryTransform.add(qry); });
		
		for (int i = 0; i < queryTransform.size(); i++) {
			try {
				ResultSet rs = st.executeQuery(queryTransform.get(i));
				String nameFileWrite = (fileWrite + i).concat(".csv");
				CSVWriter writer = new CSVWriter(new FileWriter(nameFileWrite, false));
	            writer.writeAll(rs, true); //And the second argument is boolean which represents whether you want to write header columns (table column names) to file or not.
	            writer.close();
			} catch (IOException | SQLException exc){
				System.out.println("(OperationHelper) [writeFile] exception01: "+ exc.getMessage());
			}
		}
		st.close();
		} catch(SQLException except) {
			System.out.println("(OperationHelper) [writeFile] exception02: "+ except.getMessage());
		}
	}
}