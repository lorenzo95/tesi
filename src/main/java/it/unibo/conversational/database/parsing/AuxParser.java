package it.unibo.conversational.database.parsing;

import java.sql.Connection;
import java.util.List;
import java.util.Scanner;

import it.unibo.conversational.database.help.QueryDb;
import it.unibo.conversational.database.help.Enum.TokenOperator;
import it.unibo.conversational.database.help.parsing.HelperPredicate;

public class AuxParser {
	protected static String withFind(String input, Scanner scanner) {
		final String withString = scanner.next();
		if (!withString.equals(TokenOperator.WITH.getName())) {
			scanner.close(); 
			Parser.setError("You wrote: ".concat(withString).
					concat(" while it was = ").concat(TokenOperator.WITH.getName())); return "";
		}
		final String database = scanner.next();
		Parser.setMetadataConnection(database);
		return input.replace(TokenOperator.WITH.getName(), " ").trim().replaceAll(database, " ").trim();
	}
	
	protected static String setForIndex(String input, int for_index, Connection conn) {
		if (for_index > Parser.getOperator_index()) {
		  String tempPred = HelperPredicate.parseFor(input, for_index, Parser.getLast_index(), conn);
		  if (tempPred != null) { return tempPred; }
		  if (tempPred == null || tempPred.isEmpty()) { return ""; }
		} else if (for_index > 0 && for_index <= Parser.getOperator_index()) {
			if (Parser.getError().isEmpty()) {
				Parser.setError(("For Index found in pos = " + for_index)
						.concat("while Operator Index found in pos = " + Parser.getOperator_index()));
			}
			return "";
		}
		return "1";
	}
	
	public static int byIndex(String input, Connection conn) {
		int by_using_index = input.indexOf(TokenOperator.BY.getName());
		if (by_using_index < 0) { if (Parser.getError().isEmpty()) {
			Parser.setError("IndexBy Not Properly set"); } return -1;}
		
		String groupby[] = input.substring(by_using_index, input.length()).replace(TokenOperator.BY.getName(), " ").trim().split(","); //GC 
		List<String> level_database = QueryDb.getData(conn, "level_name", "level");
			
		for(int i = 0; i < groupby.length; i++) {
			if(i > 0) { Parser.jsongroupby = Parser.jsongroupby.concat(","); }
			if(!level_database.contains(groupby[i].trim().toLowerCase())) {
				if (Parser.getError().isEmpty()) { Parser.setError("Group by level not found"); }return -1;
			}
			Parser.jsongroupby = Parser.jsongroupby.concat("\""+groupby[i].trim().toLowerCase()+"\"");
		}
		Parser.jsongroupby = Parser.jsongroupby.concat("]");
		return by_using_index;
	}
}