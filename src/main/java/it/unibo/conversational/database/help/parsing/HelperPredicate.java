package it.unibo.conversational.database.help.parsing;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import it.unibo.conversational.database.help.QueryDb;
import it.unibo.conversational.database.help.Utils;
import it.unibo.conversational.database.help.Enum.TokenOperator;
import it.unibo.conversational.database.parsing.Parser;

public class HelperPredicate {
	private static List<List<String>> synonymus;
	private static List<String> level;
	
	private static String valpredicate[];
	
	public static List<String> getLevel() { return level; }
	public static void setLevel(List<String> level) { HelperPredicate.level = level; }

	private static String findSynonym(Connection conn, 
			List<String> predicate_name, List<String> predicate_syn, 
			String predicates[], int i) {
		String pr = "";				
		for (int k = 0; pr.equals("") && k<synonymus.size(); k++) {
			if(predicates[i].contains(predicate_name.get(k))){
				valpredicate = predicates[i].split(predicate_name.get(k));
				pr = predicate_name.get(k);
			}
			for (int g = 0; pr.equals("") && g<synonymus.get(k).size(); g++) {
				if(predicates[i].contains(synonymus.get(k).get(g))){
					valpredicate = predicates[i].split(synonymus.get(k).get(g));
					pr = QueryDb.getlanOpName(conn, predicate_syn.get(k), "language_predicate");
					if (pr == null) {
						Parser.setError("Predicate synonym ".concat(predicate_syn.get(k)).concat(" not found")); 
						return "";
					}
				}
			}
		}
		return pr;
	}
	
	public static String parseFor(String input, int for_index, int last_index, Connection conn) {
		String predicate[] = input.substring(for_index, last_index).replace(TokenOperator.FOR.getName(), " ").trim().split(","); //SC
		return parsingFor(conn, predicate);
	}
	
	public static String parsingFor(Connection conn, String predicates[]) {
		valpredicate = new String[predicates.length*2];			
		List<String> predicate_name = new ArrayList<>(); List<String> predicate_syn = new ArrayList<>();
		if (!HelperMeasure.setGetPredicateSyn(predicate_name, predicate_syn, conn)) { return ""; }		
		synonymus = Utils.buildSynonymList(predicate_syn); //lista delle liste dei sinonimi
	
//		per ogni predicato nella stringa in input cerco quale predicato ho
//		scorrendo gli array prima creati, una volta trovati aggiorno il json
		String jsonpredicate = "";
		for(int i = 0; i < predicates.length; i++) {   
			if(i>0) {
				jsonpredicate = jsonpredicate.concat(",");
			}				
			String pr = findSynonym(conn, predicate_name, predicate_syn, predicates, i);
			String valTrim = "";
			try {
				valTrim = valpredicate[0].trim();
			} catch (NullPointerException exc) { Parser.setError("For index not properly set"); return ""; }
			if(valTrim.isEmpty() || !level.contains(valTrim)) { //controllo che il livello effettivamente esista
				Parser.setError("level not valid ".concat(valTrim)); return "";
			}
			if (valpredicate.length < 2) { Parser.setError("Attribute value not set "); return ""; }
			
			jsonpredicate = jsonpredicate.concat("{"
			.concat("\"VAL\":\"").concat("'").concat(valpredicate[1].trim()).concat("'").concat("\",")
			.concat("\"COP\":\"").concat(pr).concat("\",")
			.concat("\"ATTR\":\"").concat(valTrim).concat("\"")
			.concat("}"));
		}
		return jsonpredicate;
	}
}