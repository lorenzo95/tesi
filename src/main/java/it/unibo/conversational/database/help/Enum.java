package it.unibo.conversational.database.help;

public class Enum {
	public enum NumDatabase {
		FOOD_MART("foodmart"), CONVERSATIONAL("conversational"),
		COVID_MART("covid_mart"), COVID_METADATA("covid_metadata");
		
		private String name;
		NumDatabase(String name) { this.name = name; }
		public String getName() { return name; }
	}
	
	public enum TokenOperator {
		WITH("with"), EXPLAIN("explain"), DESCRIBE("describe"),
		USING("using"),
		BY("by"), FOR("for");
		
		private String name;
		TokenOperator(String name) { this.name = name; }
		public String getName() { return name; }
	}
	
	public enum MonthOfYear {
		JANUARY("january", 1), FEBRUARY("february", 2), MARCH("march", 3),
		APRIL("april", 4), MAY("may", 5), JUNE("june", 6),
		JULY("july", 7), AUGUST("august", 8), SEPTEMBER("september", 9),
		OCTOBER("october", 10), NOVEMBER("november", 11), DECEMBER("december", 12);
			
		private final static int NUM_MONTHS = 12;
		private final String name;
		private final int numMonthYear;
		
		MonthOfYear(final String name, final int numMonthYear) {
			this.name = name;
			this.numMonthYear = numMonthYear;
		}
		
		public static String nameMonth(final int numMonth) {			
			if (numMonth < 1 && numMonth > NUM_MONTHS) { return "none"; }
			MonthOfYear[] arrayMonth = MonthOfYear.values();
			int dimArrayMonth = arrayMonth.length;
			for (int i = 0; i < dimArrayMonth; i++) {
				if (numMonth == arrayMonth[i].getNumMonthYear()) {
					return arrayMonth[i].getName();
				}
			} return "";
		}
		
		public String getName() { return name; }
		public int getNumMonthYear() { return numMonthYear; }
	}
	
	public enum DayOfWeek {
		MONDAY("monday", 1), TUESDAY("tuesday", 2), WEDNESDAY("wednesday", 3),
		THURSDAY("thursday", 4), FRIDAY("friday", 5), SATURDAY("saturday", 6),
		SUNDAY("sunday", 7);
			
		private final static int NUM_DAYS = 7;
		private final String name;
		private final int numDayWeek;
		
		DayOfWeek(final String name, final int numDayWeek) {
			this.name = name;
			this.numDayWeek = numDayWeek;
		}
		
		public static String nameDayWeek(final int numDay) {			
			if (numDay < 1 && numDay > NUM_DAYS) { return "none"; }
			DayOfWeek[] arrayDay = DayOfWeek.values();
			int dimArrayDay = arrayDay.length;
			for (int i = 0; i < dimArrayDay; i++) {
				if (numDay == arrayDay[i].getNumDayWeek()) {
					return arrayDay[i].getName();
				}
			} return "";
		}
		
		public String getName() { return name; }
		public int getNumDayWeek() { return numDayWeek; }
	}
	
	public enum NameAlgorithms {
		ANOVA("Anova"), PEARSON("Pearson-Index"), TREND("Trend-detection"),
		OUTLIER("Outlier"), TOP_K("Top-k"), K_MEANS("K-means");
		private String name;
		private NameAlgorithms(final String name) { this.name = name; }
		public String getName() { return this.name; }
	}
}