package it.unibo.conversational.database.help.parsing;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import it.unibo.conversational.database.help.Enum.TokenOperator;
import it.unibo.conversational.database.parsing.AuxParser;
import it.unibo.conversational.database.parsing.Parser;

public class HelperBase {	
	private static String getQuery() {
		return "{".concat(Parser.jsonpredicate).concat(Parser.jsonmeasures).concat(Parser.jsongroupby).concat("}");
	}
	
	private static List<String> doCorrelation(Connection conn, final String partialUsing) {
		String othreMeasures[] = new String[] {partialUsing};
		Parser.jsonmeasures = Parser.jsonmeasures.concat(",");
		String lastMeasure = HelperMeasure.parsingMeasure(conn, othreMeasures);
		if (!lastMeasure.isEmpty()) {
			Parser.jsonmeasures = Parser.jsonmeasures.concat(lastMeasure);
		} else { return new ArrayList<>(); }
		return closeQuery();
	}
	
	private static List<String> doDefault(Connection conn, final String partialUsing) {
		List<String> listReturn = new ArrayList<>();
		listReturn.add(getQuery());
		
		Parser.jsonpredicate = "\"SC\":[";
		Parser.jsonpredicate = Parser.jsonpredicate.concat(HelperPredicate.parsingFor(conn, new String[] {partialUsing}));
		Parser.jsonpredicate = Parser.jsonpredicate.concat("]");

		listReturn.add(getQuery());
		return listReturn;
	}
	
	public static List<String> findExplain(String input, Scanner scanner, Connection conn) {
		int using_index = Parser.operationParsing(input, scanner, TokenOperator.EXPLAIN.getName(), conn);
		
		if (using_index > 0) {	
			int by_index = AuxParser.byIndex(input, conn);
			if (by_index < 0) { try {conn.close();} catch(SQLException e) {}; return new ArrayList<>(); }
			
			String restInput = input.substring(using_index, by_index);
			restInput = restInput.replace(")", "").replace("using", "").trim();
			String stringUsing[] = restInput.replace("(", ":").split(":");
							
			if (stringUsing[0].equals("next") || stringUsing[0].equals("past")) { // fare 2 query
				int indEq = input.indexOf("=");
				if (indEq < 0) { try {conn.close();} catch(SQLException e) {}; return new ArrayList<>(); }
				indEq++;
				int indComma = input.indexOf(",");
				final int usingIndex = input.indexOf(TokenOperator.USING.getName());
				if (indComma < 0 || indComma > usingIndex) { indComma = usingIndex; indComma--; }
				String predString = input.substring(indEq, indComma);
				return SupportHelpBase.predicateTemporal(input, stringUsing, conn, predString);
			}
			if (stringUsing[0].equals("correlation")) {
				return doCorrelation(conn, stringUsing[1]);
			} else { // fare 2 query	
				return doDefault(conn, stringUsing[0]);
			}				
		} else { 
			if (Parser.getError().isEmpty()) { 
				Parser.setError("Using index not properly set"); 
			} try {conn.close();} catch(SQLException e) {}; return new ArrayList<>(); 
		}
	}
	
	public static List<String> closeQuery() {
		Parser.jsonmeasures = Parser.jsonmeasures.concat("]");
		Parser.jsonpredicate = Parser.jsonpredicate.concat("]");		
		return Arrays.asList(getQuery());
	}
}