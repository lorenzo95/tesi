package it.unibo.conversational.database.help.parsing;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import it.unibo.conversational.database.help.QueryDb;
import it.unibo.conversational.database.help.Utils;
import it.unibo.conversational.database.parsing.Parser;

public class HelperMeasure {
	private static List<List<String>> synon_gb;
	
	private static List<String> groupbyoperator_database = new ArrayList<>();
	private static List<String> groupbyoperator_syn = new ArrayList<>();
	private static List<String> measures_database = new ArrayList<>();
	
	private static String valmeasures[];
	
	private static boolean setOperators(Connection conn) {
		if (groupbyoperator_database.isEmpty()) {
			try {
			  QueryDb.makeOperationOnDb(conn, groupbyoperator_database, groupbyoperator_syn, "groupbyoperator", "");
			} catch (Exception exc) { return false; }
		} return true;
	}
	
	public static void setMeasureDatabase(final List<String> listMeasure) { measures_database = listMeasure; }
	public static List<String> getMeasureDatabase() { return measures_database; }
	
	protected static boolean setGetPredicateSyn(List<String> predicate_name, List<String> predicate_syn, Connection conn) {
		try {
			QueryDb.makeOperationOnDb(conn, predicate_name, predicate_syn, "language_predicate", "WHERE language_predicate_type = 'predicate'");
		} catch (Exception exc) {
			Parser.setError("Predicate synonym not found"); return false;
		} return true;
	}
	
	public static String parseMeasure(String input, int operator_index, int last_index, String REPL_STRING, Connection conn) {
		String measures[]= input.substring(operator_index, last_index).replace(REPL_STRING, " ").trim().split(",");
		return parsingMeasure(conn, measures);
	}
	
	private static String getMeasure(String measures[], int i) {
		String measure = "";	
  		for(int k=0;k<synon_gb.size();k++) {   // Ricerca operatore
  			String op = groupbyoperator_database.get(k);
  			if(measures[i].contains(op)) {
  				valmeasures = measures[i].split(op); measure = op;										
  			}
  			for(int g=0;g<synon_gb.get(k).size();g++) { // Ricerca in lista sinonimi
  				String sy = synon_gb.get(k).get(g);
  				if(measures[i].contains(sy)){
  					valmeasures = measures[i].split(sy); measure = op;
  				}
  			}
  		}
  		return measure;
	}
	
	public static String parsingMeasure(Connection conn, String measures[]) {
		if (!setOperators(conn)) { Parser.setError("Operators not well set"); return ""; }
		synon_gb = Utils.buildSynonymList(groupbyoperator_syn);
			  
		String jsonmeasures = "";
	  	valmeasures = new String[measures.length*2];
	  	for(int i = 0; i < measures.length; i++) {   ////per ogni misura nella stringa in input
	  		if(i>0) { jsonmeasures = jsonmeasures.concat(","); }
	  		// Ricerca sinonimi delle misura: valmeasures
	  		String measure = getMeasure(measures, i);
								
			if (measure == "") { valmeasures[1] = measures[i]; }
			String valmeas = valmeasures[1].replace(")", " ").replace("(", " ").trim().toLowerCase();
			if(!measures_database.contains(valmeas)) {
				Parser.setError("Measure not found: ".concat(measures[i])); return "";
			} 
			jsonmeasures = jsonmeasures.concat("{"
						.concat("\"OP\":\"").concat(measure).concat("\",")
					  	.concat("\"MEA\":\"").concat(valmeas).concat("\"")
						.concat("}"));	
		}
		return jsonmeasures;
		}
}