package it.unibo.conversational.database.help.parsing;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import it.unibo.conversational.database.help.Enum.DayOfWeek;
import it.unibo.conversational.database.help.Enum.MonthOfYear;
import it.unibo.conversational.database.parsing.Parser;

public class SupportHelpBase {
	private static String nextPastPredicate = "";
	
	private static String getQuery() {
		return "{".concat(Parser.jsonpredicate).concat(Parser.jsonmeasures).concat(Parser.jsongroupby).concat("}");
	}
	
	private static boolean evaluateMonthOrYear(Connection conn,
			final String[] stringUsing, String temp,
			boolean next) {
		try {																									
			int numMonth = MonthOfYear.valueOf(temp.toUpperCase()).getNumMonthYear();
			if (next) { numMonth++; } else { numMonth--; }
			
			final String nextMonthString = MonthOfYear.nameMonth(numMonth);
			nextPastPredicate = ("the_".concat(stringUsing[1]).concat("=").concat(nextMonthString));
			return true;
		} catch (NullPointerException e) {
			try {
				int numDay = DayOfWeek.valueOf(temp).getNumDayWeek();
				if (next) { numDay++; } else { numDay--; }
				
				final String nextDayString = DayOfWeek.nameDayWeek(numDay);
				nextPastPredicate = ("the_".concat(stringUsing[1]).concat("=").concat(nextDayString));
				return true;
			} catch (NullPointerException exc) { try {conn.close(); return false;} catch(SQLException ex) {}; throw new NullPointerException(); }
		}
	}
	
	private static void setNextPredicate(Connection conn,
			final String[] stringUsing, String input, final String predValue, boolean next) {
		boolean opSet = false; int value = -1;
		try {
			value = Integer.valueOf(predValue);
		} catch (NumberFormatException excNumber) {
			opSet = evaluateMonthOrYear(conn, stringUsing, predValue, next);
		} finally {
			if (!opSet) {
				if (next && value >= 0) { value++; } if (!next && value >= 0) { value--; }
				nextPastPredicate = ("the_".concat(stringUsing[1]).concat("=")+ value);
			}
		}
	}
	
	private static String takeSecondPredicate(final String predicate) {
		final int firstGraphClose = predicate.indexOf("}");
		final int secondGraphOpen = predicate.indexOf("{", firstGraphClose);
		
		if (secondGraphOpen < 0) { return ""; }
		int lastGraphClose = predicate.lastIndexOf("}"); lastGraphClose++;
		
		return predicate.substring(secondGraphOpen, lastGraphClose);
	}
	
	public static List<String> predicateTemporal(String input, final String[] stringUsing, 
			Connection conn, final String predValue) {
		boolean next = stringUsing[0].equals("next");
		
		int indice = input.indexOf(stringUsing[1]), equlIndex = input.indexOf("=", indice);
		if (equlIndex < 0) { try {conn.close();} catch(SQLException e) {}; return new ArrayList<>(); }
		
		setNextPredicate(conn, stringUsing, input, predValue, next);
		
		Parser.jsonmeasures = Parser.jsonmeasures.concat("]");
		final String lastPredicate = takeSecondPredicate(Parser.jsonpredicate);
		Parser.jsonpredicate = Parser.jsonpredicate.concat("]");
		String firstQuery = getQuery();
		
		Parser.jsonpredicate = "\"SC\":[";
		String pred = HelperPredicate.parsingFor(conn, new String[] {nextPastPredicate});
		
		if (!pred.isEmpty()) {
			Parser.jsonpredicate = Parser.jsonpredicate.concat(pred);
			Parser.jsonpredicate = Parser.jsonpredicate.concat(",").concat(lastPredicate); // added
			Parser.jsonpredicate = Parser.jsonpredicate.concat("]");			
			String secondQuery = getQuery();
			return Arrays.asList(firstQuery, secondQuery);
		} return new ArrayList<>();
	}
}