package it.unibo.conversational.database.help;

import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import it.unibo.conversational.algorithms.Parser;
import it.unibo.conversational.database.QueryGeneratorChecker;

public class QueryDb {
	public static void driverLoad() throws InstantiationException, IllegalAccessException{
		try { Class.forName("com.mysql.jdbc.Driver").newInstance(); } 
			catch (ClassNotFoundException e) {
				throw new IllegalStateException("Cannot find the driver in the classpath!", e);
			}  // --> ok
	}
	
	public static String createQuery(JSONObject obj) {
		try (Statement stmt = QueryGeneratorChecker.getDataConnection().createStatement();) {
			String query = Parser.createQuery(obj);
			System.out.println("query = " + query);
			return query;	      
		    } catch (final Exception ee) {
		    	ee.printStackTrace();
		    	fail(ee.getMessage());
		      return null;
		    }
	}
	
	public static List<String> getData (Connection cn, String select, String from) {
		try {
			List<String> measures_database = new ArrayList<>();
			String query = "SELECT ".concat(select).concat(" FROM ").concat(from);			
		    Statement st = cn.createStatement();
		    ResultSet rs = st.executeQuery(query);		      
		    while (rs.next()) {
		        String database_name = rs.getString(select);
		        if (database_name == null){
		        	measures_database.add("null");
		        } else {
		        	measures_database.add(database_name.toLowerCase());
		        }
		    }
		    st.close();
		    return measures_database;
		} catch (SQLException sqlException) {
			return new ArrayList<>();
		}
	}
	
	public static String getlanOpName(Connection conn, String synK, String name) {
		try {
			String pr = "";
			String query = "SELECT ".concat(name).concat("_name FROM ").concat(name).concat(" WHERE ").concat(name).concat("_synonyms= '").concat(synK).concat("'");
			Statement st = conn.createStatement();
		    ResultSet rs = st.executeQuery(query);  
		    while (rs.next()){
		        String database_name = rs.getString(name.concat("_name"));
		        pr = database_name;
		    }
		    st.close();
		    return pr;
		}catch (Exception exc){
			System.out.println("Errore: "+ exc.getMessage());
			return null;
		}
	}
	
	public static void makeOperationOnDb(Connection cn, List<String> db, List<String> sn, String nameRequest, String Where) throws SQLException {
		String query = "SELECT ".concat(nameRequest).concat("_name,").concat(nameRequest).concat("_synonyms FROM ").concat(nameRequest).concat(" ").concat(Where);
		Statement st = cn.createStatement();
	    ResultSet rs = st.executeQuery(query);			      
	    while (rs.next()) {
	        String gb_name = rs.getString(nameRequest.concat("_name"));
	        String gb_syn = rs.getString(nameRequest.concat("_synonyms"));
	        db.add(gb_name);
	        if(gb_syn==null) {
	        	sn.add("null");
	        }else {
	        	sn.add(gb_syn);
	        }			        
	    }
	    st.close();
	}
}