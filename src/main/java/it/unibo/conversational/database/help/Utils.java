package it.unibo.conversational.database.help;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Utils {	
	public static String FILE = "OpenCSVWriteByResultSet";
	public static String EXTENSION = ".csv";
	
	public static List<List<String>> buildSynonymList(List<String> groupbyoperator_syn) {
		List<List<String>> synon_gb = new ArrayList<>();
		  String syno[] = new String[groupbyoperator_syn.size()*10];			
		  for (String string : groupbyoperator_syn) {
			  syno = string.split(",");
			  List<String> syn2 = new ArrayList<>();
			  for (String string2 : syno) {
				  if(string == "null") {
					  syn2.add("null");
				  } else {
					  string2 = string2.replaceAll(Pattern.quote("["), " ").replaceAll(Pattern.quote("]"), " ").trim();
					  syn2.add(string2.substring(1, string2.length()-1));
				  }
			  }
			  List<String> tmp = syn2.stream().distinct()
		                .sorted((x, y) -> Integer.compare(y.length(), x.length()))
		                .collect(Collectors.toList());
			  synon_gb.add(tmp);
		  	}
		return synon_gb;
	}
		
	public static Map<String, List<Object>> readCsvDouble(String fileName) {
		List<List<Object>> measureValues = new ArrayList<>();
		List<String> listMeasures = new ArrayList<>();
		
		int indexFILE = FILE.length();
		int indexExtension = fileName.indexOf(EXTENSION);
		String valMes = fileName.substring(indexFILE, indexExtension);
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(fileName)));  
			String line = ""; boolean firstRow = true;
			while ((line = br.readLine()) != null) {
				String[] vectorString = line.split(",");
				
				for (int i = 0; i < vectorString.length; i++) {
					String item = vectorString[i];
					
					int endString = item.length();
					item = item.substring(1, endString-1); // eliminazione apice
					
					if (firstRow) { // header column
						listMeasures.add(item.concat(valMes)+i);
						measureValues.add(i, new ArrayList<>());
					} else {
						Double doubleSd = -1.00;
						try { doubleSd = Double.valueOf(item);
				    	} catch (NumberFormatException ioExc00) {measureValues.get(i).add(item);}
						finally {if (doubleSd > 0.00) measureValues.get(i).add(doubleSd);}
					}
				}
				firstRow = false;
			}
			br.close();
		} catch (IOException ioExc01) {}
		Map<String, List<Object>> mapReturn = new HashMap<>();
		for (int i = 0; i < listMeasures.size(); i++) {
			mapReturn.put(listMeasures.get(i), measureValues.get(i));
		}
		return mapReturn;
	}
}