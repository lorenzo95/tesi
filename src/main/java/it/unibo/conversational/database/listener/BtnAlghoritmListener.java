package it.unibo.conversational.database.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import it.unibo.conversational.database.graphic.DisplayAlgorithm;
import it.unibo.conversational.database.graphic.TempGraphic;
import it.unibo.conversational.database.help.Enum.NameAlgorithms;
import it.unibo.conversational.database.listenerPanel.help.TableSelectorListener;
import it.unibo.conversational.database.myAlgorithms.PythonAlgorithm;
import it.unibo.conversational.database.myAlgorithms.Utils;

public class BtnAlghoritmListener implements ActionListener {
	private JTable jtable;
	private final static int k_FINAL = 5;
	public BtnAlghoritmListener(JTable jtable) { this.jtable = jtable; }
		
	@Override
	public void actionPerformed(ActionEvent arg0) {
		String btnLabel = arg0.getActionCommand();						
		List<Map<String, List<Object>>> mapreturn = TempGraphic.getListMap();
		int numCluster = TempGraphic.getNumCluster();
		
		if (btnLabel.equals(NameAlgorithms.OUTLIER.getName())) { doOutlier(mapreturn);
		} else if (btnLabel.equals(NameAlgorithms.TOP_K.getName())) { doTopK(numCluster, mapreturn);
		} else if (btnLabel.equals(NameAlgorithms.ANOVA.getName())) { doAnova();
		} else if (btnLabel.equals(NameAlgorithms.PEARSON.getName())) { doPearsonIndex();
		} else if (btnLabel.equals(NameAlgorithms.K_MEANS.getName())) { doKmeans(numCluster);
		} else if (btnLabel.equals(NameAlgorithms.TREND.getName())) { doTrendDetection();}
	}

	private void displayList(List<Object> list, final String NAME_ALGORITHM) {
		int numColumn = jtable.getColumnCount(), numRow = jtable.getRowCount(), i, j = 0;

		for (i = 0; i < numColumn; i++) {
			String nameHeader = (String) jtable.getColumnModel().getColumn(i).getHeaderValue();
			if (nameHeader.isEmpty()) { break; }
			if (nameHeader.equals(NAME_ALGORITHM) && !NAME_ALGORITHM.equals(NameAlgorithms.K_MEANS.getName())) { i++; break;}
		}

		if (i > 0) { // si suppone che ci sia almeno 1 dato
			if (i >= numColumn) { 
				DefaultTableModel modelD = (DefaultTableModel) jtable.getModel();				
				List<String> headerValues = new ArrayList<>();
				for (j = 0; j < numColumn; j++) {
					headerValues.add((String) jtable.getColumnModel().getColumn(j).getHeaderValue());
				}
				modelD.addColumn(NAME_ALGORITHM);
				for (int k = 0; k < i; k++) {
					jtable.getColumnModel().getColumn(k).setHeaderValue(headerValues.get(k));
				}
			}
			jtable.getColumnModel().getColumn(i).setHeaderValue(NAME_ALGORITHM);
			jtable.getModel().addTableModelListener(new TableSelectorListener(jtable));
			
			for (j = 0; j < list.size(); j++) { 
				Object obj = list.get(j);
				jtable.setValueAt(obj, j, i);
				
				String objString = String.valueOf(obj);
				if (objString.equals("si")) {
					jtable.setRowSelectionInterval(j, j);
				}
			}
			for (; j < numRow; j++) { jtable.setValueAt("", j, i); }
		}
		jtable.getParent().getParent().repaint();		
	}

	private void doOutlier(List<Map<String, List<Object>>> mapReturn) {			
		mapReturn.forEach(mapTemp -> {
			mapTemp.keySet().forEach(key -> {
				List<Double> listData = Utils.getListDouble(mapTemp.get(key));
				if (!listData.isEmpty()) {
					List<Double> getOutLiers = Utils.getOutliers(listData);
					List<Object> listObj = new ArrayList<>();
					listData.forEach(e -> {
						if (getOutLiers.contains(e)) { listObj.add("si");
						} else { listObj.add("no"); }
					});
					displayList(listObj, NameAlgorithms.OUTLIER.getName());
				}
			});
		});
	}
	
	private void doTopK(final int numK, List<Map<String, List<Object>>> mapReturn) {
		try {		
			mapReturn.forEach(mapTemp -> {
				final int numTopK; if (numK > 0) { numTopK = numK; } else { numTopK = k_FINAL; }
				mapTemp.keySet().forEach(key -> {
					List<Double> listDouble = Utils.takeFirstKListDouble(mapTemp.get(key), numTopK);
					if (!listDouble.isEmpty()) {
						List<Object> listObj = listDouble.stream().map(e -> e).collect(Collectors.toList());
						displayList(listObj, NameAlgorithms.TOP_K.getName());
					}
				});
			});
		} catch (Exception e) { System.out.println("BtnAlghoritmListener [doTopK] ".concat(e.getMessage())); }
	}

	private void doKmeans(int numCluster) {
		try {
			List<Object> listBelongCluster = DisplayAlgorithm.computationKMeans(numCluster);
			displayList(listBelongCluster, NameAlgorithms.K_MEANS.getName());
		} catch (Exception e) {
			System.out.println("BtnAlghoritmListener [doKmeans] ".concat(e.getMessage()));
		}
	}

	private void doTrendDetection() { PythonAlgorithm.doComputation(NameAlgorithms.TREND.getName(), false); }
	private void doAnova() { PythonAlgorithm.doComputation(NameAlgorithms.ANOVA.getName(), true); }
	private void doPearsonIndex() { PythonAlgorithm.doComputation(NameAlgorithms.PEARSON.getName(), true); }
}