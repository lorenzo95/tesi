package it.unibo.conversational.database.listener.saveLoad;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import it.unibo.conversational.database.parsing.OperationHelper;

public class ButtonSave implements ActionListener {
	private static String fileQuerySave = "ListQuery.txt";
	private String intention;
	
	private boolean intentionExist() {
		try {
			BufferedReader out = new BufferedReader(new FileReader(fileQuerySave));
			String ret = "";
			while((ret = out.readLine()) != null) {
				if (intention.equals(ret)) { out.close(); return true; }
			}
			out.close();
		} catch (IOException exc) {
			System.out.println("Eccezione = ".concat(exc.getMessage()));
		}
		return false;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {		
		try {			
			intention = OperationHelper.getIntention();
			if (intentionExist()) { return; }
			
			BufferedWriter writer = new BufferedWriter(new FileWriter(fileQuerySave, true));
			writer.append(intention.concat("\n")); writer.close();
		} catch (IOException exc) {
			System.out.println("Eccezione = ".concat(exc.getMessage()));
		}
	}
}