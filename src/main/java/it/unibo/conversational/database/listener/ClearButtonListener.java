package it.unibo.conversational.database.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;

import it.unibo.conversational.database.graphic.TempGraphic;

public class ClearButtonListener implements ActionListener {
	private JPanel commandPanel;
	private JTextArea msgArea;
	private JTable jtable;

	public ClearButtonListener(JPanel commandPanel, JTextArea msgArea, JTable jtable) {
		this.commandPanel = commandPanel;
		this.msgArea = msgArea;
		this.jtable = jtable;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		TempGraphic.cleanGraphic(commandPanel, msgArea, jtable);
	}
}
