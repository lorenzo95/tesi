package it.unibo.conversational.database.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import it.unibo.conversational.database.graphic.TempGraphic;
import it.unibo.conversational.database.help.Utils;
import it.unibo.conversational.database.parsing.OperationHelper;
import it.unibo.conversational.database.parsing.Parser;

public class SubmitButtonListener implements ActionListener {	
	public JPanel commandPanel;
	public JTextArea msgArea;
	public static JTextArea givenTextArea;
	public JTable jtable;
					
	public SubmitButtonListener(JTextArea givenString, JPanel commandPanel, JTextArea msgArea, JTable jtable) {
		givenTextArea = givenString;
		this.commandPanel = commandPanel;
		this.msgArea = msgArea;
		this.jtable = jtable;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		TempGraphic.cleanGraphic(commandPanel, msgArea, jtable);
		String parsingString = givenTextArea.getText().toLowerCase();
		List<String> queryArray = OperationHelper.getQuery(jtable, commandPanel, parsingString);
		
		if (queryArray != null && !queryArray.isEmpty()) {
			OperationHelper.writeFile(queryArray, Utils.FILE);					
			for (int i = 0; i < queryArray.size(); i++) {
				TempGraphic.showData(Utils.FILE.concat(i+Utils.EXTENSION), jtable);
			}
			
			queryArray.stream().filter(qry -> qry != null && !qry.isEmpty()).forEach(qry -> {
				msgArea.append(qry.concat("\n"));
			});
		} else { msgArea.append("Query to DB not succesfull ".concat(Parser.getError())); }
	}
}