package it.unibo.conversational.database.myAlgorithms;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Lorenzo
 */
public class PythonKmeans {
	public static final String pathFile = "C:/Users/Lorenzo/Documents/UNIBO-Magistrale/Tesi/tesi/";
	public static final String command = "cmd /c python ";
	public static final String execFile = pathFile.concat("k-means.py");
	public static final String outFile = "Dati_Out0.txt";
		
	private static final char _2point = ':';
	
	public static List<String> getKmeans(int value) {
		String param = " ";
		if (value > 1) { param += value; } else { param += "3"; }
		
		List<String> listCluster = new ArrayList<>();
		try {			
			Process p = Runtime.getRuntime().exec(command.concat(execFile).concat(param));
			p.waitFor();
			
			String ret;
			BufferedReader out = new BufferedReader(new FileReader(outFile));
			while((ret = out.readLine()) != null) {
				int indexR = ret.lastIndexOf(_2point) + 1;
				String lastString = ret.substring(indexR, ret.length());				
				listCluster.add(lastString);
			}
			
			out.close();
		} catch (InterruptedException | IOException e) {
			System.out.println("Eccezione = " + e.getMessage());
		}
		return listCluster;
	}
	
	public static void main(String... args) {		
		List<String> listIndex = getKmeans(4);
		int dim = listIndex.size();
		for (int cluster = 0; cluster < dim; cluster++) {
			System.out.println("cluster = " + cluster + " " + listIndex.get(cluster));
		}
	}
}