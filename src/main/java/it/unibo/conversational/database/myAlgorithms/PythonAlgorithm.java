package it.unibo.conversational.database.myAlgorithms;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.unibo.conversational.database.graphic.help.GraphicApp;
import it.unibo.conversational.database.help.Enum.NameAlgorithms;

public class PythonAlgorithm {
	public static final String pathFile = "C:/Users/Lorenzo/Documents/UNIBO-Magistrale/Tesi/tesi/";
	public static final String command = "cmd /c python ";
	public static final String execFileAnova = pathFile.concat("anova.py");
	public static final String execFilePearson = pathFile.concat("pearsonIndex.py");
	public static final String execFileTrend = pathFile.concat("trend_detection.py");
	public static final String outFile = "Dati_Out0.txt";
	
	public static String value = "value";
	
	private static List<String> makeList() {
		List<String> listValues = new ArrayList<>();
		String ret;
		try {
			BufferedReader out = new BufferedReader(new FileReader(outFile));
			while((ret = out.readLine()) != null) {
				int valueLen = value.length() + 1;
				
				int indexNum = ret.indexOf(value) + valueLen;
				int stringLen = ret.length();
				String valueStr = ret.substring(indexNum, stringLen);
				
				listValues.add(valueStr);
			}
			out.close();
		} catch (IOException e) {
			System.out.println("Eccezione = " + e.getMessage());
		}
		return listValues;
	}
	
	private static void makeOutput() {
		try {
			BufferedReader out = new BufferedReader(new FileReader(outFile));
			String ret = "\n";
			while((ret = out.readLine()) != null) {
				GraphicApp.getMsgArea().append(ret.concat("\n"));
			}
			out.close();
		} catch (IOException e) {
			System.out.println("Eccezione = " + e.getMessage());
		}
	}
	
	public static List<String> doComputation(final String operation, final boolean flagMakeList) {
		String param = "";
		Process p;
		String fileExec = "";
		
		try {
			if (operation.equals(NameAlgorithms.ANOVA.getName())) {
				fileExec = execFileAnova;
			} else if (operation.equals(NameAlgorithms.PEARSON.getName())) {
				fileExec = execFilePearson;
			} else if (operation.equals(NameAlgorithms.TREND.getName())) {
				fileExec = execFileTrend;
			}
			
			p = Runtime.getRuntime().exec(command.concat(fileExec).concat(param));
			p.waitFor();
		} catch (InterruptedException | IOException e) {
			System.out.println("Eccezione = " + e.getMessage());
		}
		if (flagMakeList) { makeOutput(); return makeList(); }
		return new ArrayList<>();
	}
	
	public static void main(String[] args) {
		List<String> listValues = doComputation(NameAlgorithms.ANOVA.getName(), true);
		listValues.forEach(el -> { System.out.println(el); });
	}
}