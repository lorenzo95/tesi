package it.unibo.conversational.database.myAlgorithms;

import java.util.ArrayList;
import java.util.List;

public class Utils {
	private static double getMean(List<Double> values){
        if(values.isEmpty()){ return 0; }
        return values.stream().reduce((a, b) -> a + b).get() / values.size();
    }

	private static double getStdDev(List<Double> values, double mean){
        if(values.isEmpty()){ return Double.POSITIVE_INFINITY; }
        double variance = values.stream().map(v -> v - mean).map(v -> v * v).reduce((a, b) -> a + b).get() / values.size();
        return Math.sqrt(variance);
    }
	
	public static List<Double> getListDouble(List<Object> tempList) {		
		List<Double> listDb = new ArrayList<>();
		tempList.stream().filter(el -> el instanceof Number).mapToDouble(elem -> 
		Double.valueOf(((Number)elem).doubleValue())).forEach(el -> listDb.add(el));
		return listDb;
	}
	
	public static List<Double> takeFirstKListDouble(List<Object> listObj, int k) {
		List<Double> listTempDouble = getListDouble(listObj);
		List<Double> listReturnDouble = new ArrayList<>();
		
		int dimList = listTempDouble.size();
		int numIter = k < dimList ? k : dimList;
		
		for (int i = 0; i < numIter; i++) {
			double max = listTempDouble.stream().mapToDouble(a -> a).max().orElse(-1.00);
			listTempDouble.remove(max);
			listReturnDouble.add(max);
		}
		return listReturnDouble;
	}
	
	public static List<Double> getOutliers(List<Double> input) {
    	List<Double> outliers = new ArrayList<Double>();
    	double mean = getMean(input);
    	double stdDev = getStdDev(input, mean);

    	input.stream().filter(el -> Math.abs(el - mean) > 2 * stdDev).forEach(el -> { 
    		outliers.add(el); 
    	});
    	return outliers;
    }
}