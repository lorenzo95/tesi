package it.unibo.conversational.database.listenerPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import it.unibo.conversational.database.graphic.TempGraphic;
import it.unibo.conversational.database.graphic.help.GraphicApp;

public class ModelInsertListener implements ActionListener {	
	@Override
	public void actionPerformed(ActionEvent e) {
		String itemSelected = TempGraphic.getString(e);
		itemSelected = itemSelected.replaceAll("[\\[\\]]", "");
		GraphicApp.getResponse().setText(itemSelected);
	}
}
