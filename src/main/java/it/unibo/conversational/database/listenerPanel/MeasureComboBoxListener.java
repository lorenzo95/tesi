package it.unibo.conversational.database.listenerPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import it.unibo.conversational.database.graphic.TempGraphic;
import it.unibo.conversational.database.graphic.help.GraphicApp;
import it.unibo.conversational.database.graphic.help.GraphicAux;
import it.unibo.conversational.database.help.Enum.NumDatabase;
import it.unibo.conversational.database.parsing.OperationHelper;

public class MeasureComboBoxListener implements ActionListener {
	private static void doAction(final JComboBox<String> tempBox, final String item) {
		String intentionEvaluate = GraphicApp.getResponse().getText().toLowerCase();

		int i, numItems = tempBox.getItemCount(); boolean sostituzione = false;
		for (i = 1; i < numItems && !sostituzione; i++) {
			String textTmp = String.valueOf(tempBox.getItemAt(i));
			
			if (intentionEvaluate.contains(textTmp)) {
				intentionEvaluate = intentionEvaluate.replace(textTmp, item);
				sostituzione = true;
			}
		}
				
		if (item.equals(NumDatabase.FOOD_MART.getName()) || 
				item.equals(NumDatabase.COVID_MART.getName())) {
			OperationHelper.setConnectionData(item);
			
			String conversional = (item.equals(NumDatabase.FOOD_MART.getName())) ? 
					NumDatabase.CONVERSATIONAL.getName() : NumDatabase.COVID_METADATA.getName();
			OperationHelper.setConnectionMetaData(conversional);
			GraphicAux.resetMeasurePredicate();
		}
		
		if (!sostituzione) { intentionEvaluate = intentionEvaluate.concat(" ".concat(item)); }
		GraphicApp.getResponse().setText(intentionEvaluate);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {		
		final String itemSelected = TempGraphic.getString(e);
		final JComboBox<String> tempBox = (JComboBox<String>) e.getSource();
		doAction(tempBox, itemSelected);
	}
}
