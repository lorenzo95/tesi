package it.unibo.conversational.database.listenerPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import it.unibo.conversational.database.graphic.TempGraphic;
import it.unibo.conversational.database.graphic.help.ComboBoxPredicate;
import it.unibo.conversational.database.graphic.help.GraphicApp;

public class ComboBoxListener implements ActionListener {
	private static String FOR = "for";
	private static String USING = "using";
	private static String BY = "by";
	
	private void doAction(final ComboBoxPredicate<String> tempBox, final String item, final boolean typeAction) {
		String intentionEvaluate = GraphicApp.getResponse().getText().toLowerCase();

		if (typeAction) {			
			if (intentionEvaluate.indexOf(FOR) > 0) {
				int firstIndex = intentionEvaluate.indexOf(FOR);
				firstIndex += FOR.length() + 1;
				
				int endIndex = -1;
				if (intentionEvaluate.indexOf(USING) > 0) {
					endIndex = intentionEvaluate.indexOf(USING) - 1;
				} else if (intentionEvaluate.indexOf(BY) > 0) {
					endIndex = intentionEvaluate.indexOf(BY) - 1;
				} else {
					endIndex = intentionEvaluate.length();
				}
				String stringPredicate = intentionEvaluate.substring(firstIndex, endIndex); 
				intentionEvaluate = intentionEvaluate.replace(stringPredicate, item);
				
			} else {
				intentionEvaluate = intentionEvaluate.concat(" ".concat(item));
			}
		} else {
			if (intentionEvaluate.indexOf(BY) > 0) {
				int endIndex = intentionEvaluate.lastIndexOf(' ');
				intentionEvaluate = intentionEvaluate.substring(0, endIndex);
				intentionEvaluate = intentionEvaluate.concat(" ".concat(item));
			} else {
				intentionEvaluate = intentionEvaluate.concat(" ".concat(item));
			}
		}
		GraphicApp.getResponse().setText(intentionEvaluate);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {		
		final String itemSelected = TempGraphic.getString(e);
		final ComboBoxPredicate<String> tempBox = (ComboBoxPredicate<String>) e.getSource();
		doAction(tempBox, itemSelected, tempBox.getIdentifier().equals("predicate"));
	}
}