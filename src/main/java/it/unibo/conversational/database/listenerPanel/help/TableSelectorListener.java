package it.unibo.conversational.database.listenerPanel.help;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

public class TableSelectorListener implements TableModelListener {
	private JTable jtable;
	
	public TableSelectorListener(final JTable jtable) {
		this.jtable = jtable;
	}
	
	@Override
	public void tableChanged(TableModelEvent e) {
		int rowIndex = jtable.getSelectedRow(), columnIndex = jtable.getSelectedColumn();
		jtable.changeSelection(rowIndex, columnIndex, false, false);
	}
}