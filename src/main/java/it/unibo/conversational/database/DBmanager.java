package it.unibo.conversational.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import it.unibo.conversational.algorithms.utils.ModuleRead;

/** Handling connection to database. */
public class DBmanager {

  /** Init dbmanager. */
  protected DBmanager() {
  }

  /**
   * @param s string
   * @return append _id to the string
   */
  protected static String id(final String s) {
    return s + "_id";
  }

  /**
   * @param s string
   * @return append _name to the string
   */
  protected static String name(final String s) {
    return s + "_name";
  }

  /**
   * @param s string
   * @return append _type to the string
   */
  protected static String type(final String s) {
    return s + "_type";
  }

  /**
   * @param s string
   * @return append _synonyms to the string
   */
  protected static String synonyms(final String s) {
    return s + "_synonyms";
  }

  private static Connection connMetaSchema;
  private static Connection connDataSchema;

  public static final Connection getConnection(final String schemadb) {
    final String ip = ModuleRead.credentialsFromFile()[0];
    final String port = ModuleRead.credentialsFromFile()[1];
    final String username = ModuleRead.credentialsFromFile()[2];
    final String password = ModuleRead.credentialsFromFile()[3];
    final String host = "jdbc:mysql://" + ip + ":" + port;
    final String schemaDBstringConnection = host + "/" + schemadb + "?serverTimezone=UTC"; // modifica necessaria
    Connection conn = null;
    
    try {
      Class.forName("com.mysql.cj.jdbc.Driver");
      conn = DriverManager.getConnection(schemaDBstringConnection, username, password);
    } catch (final Exception e) {
      e.printStackTrace();
    }
    return conn;
  }

  public static final Connection getMetaConnection() {
    if (connMetaSchema == null) {
      connMetaSchema = getConnection(ModuleRead.credentialsFromFile()[6]);
    }
    return connMetaSchema;
  }

  public static final Connection getDataConnection() {
    if (connDataSchema == null) {
      connDataSchema = getConnection(ModuleRead.credentialsFromFile()[4]);
    }
    return connDataSchema;
  }

  /**
   * Execute the query and return a result.
   * @param query query to execute
   */
  public static final void executeQuery(final String query) {
    try (PreparedStatement pstmt = getMetaConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
      pstmt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * Execute the query and return an integer.
   * @param query query to execute
   * @return integer
   */
  public static final int executeQueryReturnID(final String query) {
    try (PreparedStatement pstmt = getMetaConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
      pstmt.executeUpdate();
      final ResultSet generatedKeys = pstmt.getGeneratedKeys();
      generatedKeys.next();
      return generatedKeys.getInt(1);
    } catch (final SQLException e) {
      e.printStackTrace();
      return -1;
    }
  }
}
