package it.unibo.conversational.web;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.Pair;

import it.unibo.conversational.Validator;
import it.unibo.conversational.algorithms.Parser;
import it.unibo.conversational.database.QueryGeneratorChecker;
import it.unibo.conversational.datatypes.Mapping;

/**
 * Servlet interface to conversational BI.
 */
@WebServlet("/Conversational")
public class MainServlet extends HttpServlet {
  private static final String PLACEHOLDER_INNER = "$$$placeholderinside$$$";
  private static final String PLACEHOLDER_OUTER = "$$$thisisaplaceholder$$$";
  private static final String PLACEHOLDER_CODE = "$$$placeholdercoderesponse$$$";
  private static final long serialVersionUID = 1L;
  private static final int OK = 200;
  private static final int ERROR = 500;
  private QueryGeneratorChecker checker;

  /**
   * Instantiate the servlet.
   */
  public MainServlet() {
  }

  /**
   * Given a sentence returns the string representing the parsing tree.
   * @param request request
   * @param response response
   * @throws ServletException in case of error
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  @SuppressWarnings("static-access")
protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException {
    response.addHeader("Access-Control-Allow-Origin", "*");
    response.addHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
    response.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, X-Auth-Token");
    response.setCharacterEncoding("UTF-8");
    try {
      final String actionType = request.getParameter("type");
      final String nlp = request.getParameter("nlp");
      if (actionType.equals("NLP")) { // premo run nella gui web
        final int k = Integer.parseInt(request.getParameter("k"));
        final List<Pair<Mapping, Mapping>> result = Validator.parseAndTranslate(nlp, k);
        final List<Mapping> interpretations = result.stream().map(p -> p.getLeft()).sorted((m1, m2) -> -Mapping.compareMappings(m1, m2)).collect(Collectors.toList());
        // Utils.writeParsing("result_interpretation", "S_" + nlp, interpretations);
        final String res = interpretations.stream()
            .map(s -> {
              try {
                return "id,value\n" + Mapping.toCsv(s, nlp) + PLACEHOLDER_INNER + Parser.getSQLQuery(s);
              } catch (final Exception e) {
                e.printStackTrace();
              }
              return "";
            })
            .reduce((s1, s2) -> s1 + PLACEHOLDER_OUTER + s2).get(); // and merge the charts with a placeholder
        response.setStatus(OK);
        response.getOutputStream().print(res + PLACEHOLDER_CODE + Parser.getPartOfResult(interpretations.get(0).ngrams));
        checker.saveQuery(nlp, "", "", "");
      } else { // do l'ok per inviare l'interpretazione della query con `id`
        String msg = "";
        final String mc = request.getParameter("select");
        final String gbc = request.getParameter("gbset");
        final String sc = request.getParameter("predicate");
        checker.saveQuery(nlp, gbc, sc, mc);
        final Mapping correctQuery = Validator.getBest(gbc, sc, mc);
        msg = Parser.getSQLQuery(correctQuery);
        // final Pair<Mapping, String> correctResult = Mapper.analyzeCorrectresult(gbc, sc, mc, checker);
        // if (correctResult.getLeft() == null) {
        //  msg = correctResult.getRight();
        // } else {
        //  checker.saveQuery(nlp, gbc, sc, mc);
        //  final Mapping correctQuery = Parser.parse(correctResult.getLeft()).get();
        //  msg = Parser.getSQLQuery(correctQuery);
        // }
        response.setStatus(OK);
        response.getOutputStream().print(msg);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      response.setStatus(ERROR);
    }
  }

  /**
   * Given a sentence returns the string representing the parsing tree.
   * @param request request
   * @param response response
   * @throws ServletException in case of error
   */
  @Override
  protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException {
    doGet(request, response);
  }
}
