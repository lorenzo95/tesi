package it.unibo.conversational.algorithms;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import it.unibo.conversational.algorithms.utils.DataType;
import it.unibo.conversational.database.QueryGeneratorChecker;
import it.unibo.conversational.datatypes.Entity;
import it.unibo.conversational.datatypes.Mapping;
import it.unibo.conversational.datatypes.Ngram;
import it.unibo.conversational.algorithms.utils.AnnotationType;
import it.unibo.conversational.algorithms.utils.Rules;
import it.unibo.conversational.algorithms.utils.Type;

/**
 * Handling grammar and paring.
 */
public final class Parser {

  private static Map<String, Set<Entity>> operatorOfMeasure = QueryGeneratorChecker.getOperatorOfMeasure();
  private static Map<String, Set<Entity>> membersofLevels = QueryGeneratorChecker.getMembersOfLevels();
  private static Map<String, Set<Entity>> levelsOfMembers = QueryGeneratorChecker.getLevelsOfMembers();
  private static Set<Entity> yearLevels = QueryGeneratorChecker.getYearLevels();

  private Parser() {
  }

  /**
   * Parse a mapping.
   * @param mapping mapping to be translated
   * @return parsing interpretations sorted by number of matched entities
   */
  public static Optional<Mapping> parse(final Mapping mapping) {
    final Set<Mapping> res = parse(Lists.newArrayList(//
        new Rules[] { Rules.M1, Rules.M2, Rules.M5, Rules.M3, Rules.M4 }, //
        new Rules[] { Rules.G1, Rules.G2 }, //
        new Rules[] { Rules.S1, Rules.S2, Rules.S3, Rules.S4, Rules.S5, Rules.S6, Rules.S7 }, //
        new Rules[] { Rules.Q1, Rules.Q2, Rules.Q3, Rules.Q4, Rules.Q5, Rules.Q6, Rules.Q7, Rules.Q8, Rules.Q9, Rules.Q10, Rules.Q11 }//
    ), mapping);
    return res.isEmpty() ? Optional.empty()
        : res.stream() //
            .filter(s -> s.ngrams.stream().anyMatch(n -> n.type.equals(Type.GPSJ))) //
            .max(Mapping::compareMappings);
  }

  public static void typeCheck(final Mapping m) {
    for (Ngram n : m.ngrams.stream().filter(n -> !n.children.isEmpty()).collect(Collectors.toList())) {
      typeCheck(n, operatorOfMeasure, membersofLevels, levelsOfMembers);
    }
  }

  /**
   * Type check the generated parsing tree and annotate the ngrams that do not satisfy the type checking.
   * @param ngram parse tree
   * @param mea2op operator/measure constraints
   * @param attr2val attribute/value constraints
   * @param val2attr value/attribute constraints
   * @return the annotated parse tree
   */
  public static Ngram typeCheck(final Ngram ngram,
      final Map<String, Set<Entity>> mea2op,
      final Map<String, Set<Entity>> attr2val,
      final Map<String, Set<Entity>> val2attr) {
    final Set<Ngram> clauses = Ngram.simpleClauses(ngram);
    for (final Ngram c : clauses) {
      switch (c.type) {
      case MC:
        if (c.children.size() == 2) { // the clause contains (operator + measure | count fact )
          final Optional<Ngram> op = c.children.stream().filter(cc -> cc.type.equals(Type.OP)).findAny();
          if (op.isPresent()) { // (operator + measure)
            final Ngram mea = c.children.stream().filter(cc -> cc.type.equals(Type.MEA)).findAny().get();
            if (!mea2op.isEmpty() && !mea2op.get(mea.mde().nameInTable()).contains(op.get().mde())) {
              c.annotate(AnnotationType.MDMV, mea2op.get(mea.mde().nameInTable()));
            }
          } else {
            // TODO do nothing, count can only be applied to fact (for now)
          }
        }
        break;
      case GC: // TODO MDGV is not implemented
        break;
      case SC:
        if (c.children.size() >= 2) { // the clause contains both a level and a value
          final Ngram lev = c.children.stream().filter(cc -> cc.type.equals(Type.ATTR)).findAny().get();
          final Ngram val = c.children.stream().filter(cc -> cc.type.equals(Type.VAL)).findAny().get();
          if (!lev.typeInDB().equals(val.typeInDB()) // if the types differ or the member is not in the domain of the level
              || !lev.typeInDB().equals(DataType.NUMERIC) && val.mde().refToOtherTable() != lev.mde().pkInTable()) {
            c.annotate(AnnotationType.AVM, attr2val.getOrDefault(lev.mde().nameInTable(), Sets.newLinkedHashSet()));
          }
        }
        break;
      default: 
        throw new IllegalArgumentException("Type checking cannot be applied to " + c);
      }
    }
    return ngram;
  }

  public static void infer(final Mapping m) {
    for (Ngram n : m.ngrams) {
      infer(n, operatorOfMeasure, membersofLevels, levelsOfMembers, yearLevels);
    }
  }

  /**
   * Infer missing information and add it to the parsing tree.
   * @param ngram parse tree
   * @param mea2op operator/measure constraints
   * @return the annotated and expanded parse tree
   */
  public static Ngram infer(final Ngram ngram, final Map<String, Set<Entity>> mea2op,
      final Map<String, Set<Entity>> attr2val,
      final Map<String, Set<Entity>> val2attr,
      final Set<Entity> dateAttributes) {
    final List<Ngram> clauses = Ngram.simpleClauses(ngram).stream().collect(Collectors.toList());
    for (int i = 0; i < clauses.size(); i++) {
      final Ngram c = clauses.get(i);
      switch (c.type) {
      case MC:
        if (c.children.size() == 1) { // the clause contains only a measure, add the operator
          final Ngram mea = c.children.stream().filter(cc -> cc.type.equals(Type.MEA)).findAny().get();
          final Set<Entity> appliableOperators = mea2op.get(mea.mde().nameInTable());
          if (appliableOperators.size() != 1) {
            c.annotate(AnnotationType.MA, appliableOperators);
          } else {
            final Entity operator = appliableOperators.stream().findAny().get();
            final Ngram op = new Ngram(operator.nameInTable(), Type.OP, operator, mea.pos());
            c.setChildren(ImmutableList.of(op, mea));
          }
        }
        break;
      case GC: // do nothing
        break;
      case SC:
        if (c.children.size() == 2) { // the clause contains both a level and a value
          final Ngram lev = c.children.stream().filter(cc -> cc.type.equals(Type.ATTR)).findAny().get();
          final Ngram val = c.children.stream().filter(cc -> cc.type.equals(Type.VAL)).findAny().get();
          final Ngram eq = new Ngram("=", Type.COP, new Entity("="), lev.pos());
          c.setChildren(ImmutableList.of(lev, eq, val));
        } else if (c.children.size() == 1) { // the clause contains only a value
          // If you are here, you are either:
          // - a member NUMERIC/DATE member
          // - a member from a categorical attribute with the reference to the corresponding level
          final Ngram val = c.children.stream().filter(cc -> cc.type.equals(Type.VAL)).findAny().get();
          final Ngram eq = new Ngram("=", Type.COP, new Entity("="), val.pos());
          switch (val.typeInDB()) {
          case NUMERIC:
            final double value = Double.parseDouble(val.tokens);
            if (value >= 1900 && value <= 2155) { // you are a date (boundaries are compliant with year type in MySQL)
              final Entity levEntity = dateAttributes.stream().findAny().get(); // TODO an entity is picked randomly
              final Ngram lev = new Ngram(levEntity.nameInTable(), Type.ATTR, levEntity, val.pos());
              c.setChildren(ImmutableList.of(lev, eq, val));
            } else {
              throw new IllegalArgumentException("What should I do with dangling value " + val + " ?");
            }
            break;
          case DATE:
            throw new IllegalArgumentException("What should I do with dangling date " + val + " ?");
          case STRING:
            final Set<Entity> candidates = val2attr.get(val.mde().nameInTable());
            if (candidates != null && !candidates.isEmpty()) {
              if (candidates.size() > 1) {
                c.annotate(AnnotationType.AA, candidates);
              } else {
                final Entity levEntity = candidates.stream().findAny().get();
                final Ngram lev = new Ngram(levEntity.nameInTable(), Type.ATTR, levEntity, val.pos());
                c.setChildren(ImmutableList.of(lev, eq, val));
              }
            }
            break;
          default:
            throw new NotImplementedException("This case is not handled: " + val);
          }
        }
        break;
      default:
        throw new IllegalArgumentException("Inference cannot be applied to " + c);
      }
    }
    return ngram;
  }

  public static void disambiguate(final Mapping ambiguousMapping) {
    final Optional<Ngram> annotatedNgram = ambiguousMapping.getAnnotatedNgrams().stream().findFirst();
    if (annotatedNgram.isPresent()) {
      final Ngram n = annotatedNgram.get();
      if (n.getAnnotations().size() > 1) {
        throw new IllegalArgumentException("Multiple annotations: " + n.getAnnotations());
      }
      for (Entry<AnnotationType, Set<Entity>> annotation : n.getAnnotations().entrySet()) {
        // TODO: this value is now taken in lexicographical order, but it should be chosen by the user
        if (annotation.getValue().isEmpty()) {
          continue; // I found an annotation but not valid values (e.g., this is not a categorical level)
        }
        final Entity value = annotation.getValue().stream().sorted((n1, n2) -> n1.nameInTable().compareTo(n2.nameInTable())).findFirst().get();
        final List<Ngram> prev = Lists.newLinkedList(n.children);
        switch (annotation.getKey()) {
        // TODO: for each annotation you should ask to the user what he desires. Now it is assumed automatically.
        case AVM:
          final Ngram lev = prev.stream().filter(nn -> nn.type.equals(Type.ATTR)).findAny().get();
          final Ngram cop = prev.stream().filter(nn -> nn.type.equals(Type.COP)).findAny().get();
          n.setChildren(ImmutableList.of(lev, cop, new Ngram(value.nameInTable(), Type.VAL, value, 1.0, Pair.of(-1, -1))));
          break;
        case MA:
          prev.add(0, new Ngram(value.nameInTable(), Type.OP, value, 1.0, Pair.of(-1, -1)));
          n.setChildren(ImmutableList.copyOf(prev));
          break;
        case AA:
          prev.add(0, new Ngram(value.nameInTable(), Type.ATTR, value, 1.0, Pair.of(-1, -1)));
          prev.add(1, new Ngram("=", Type.ATTR, new Entity("="), 1.0, Pair.of(-1, -1)));
          n.setChildren(ImmutableList.copyOf(prev));
          break;
        default:
          throw new NotImplementedException("This is not implemented: " + annotation);
        }
      }
      n.dropAnnotations();
    }
  }
  /**
   * Parse a mapping.
   * @param rules list of rules to apply in the parsing
   * @param mapping mapping to be translated
   * @return parsing interpretations sorted by number of matched entities
   */
  public static Set<Mapping> parse(final List<Rules[]> rules, final Mapping mapping) {
    // stack of translated sentences
    final Set<Mapping> t = Sets.newLinkedHashSet();
    // init the sentence search space
    final Stack<Mapping> s = new Stack<Mapping>();
    s.push(mapping);
    while (!s.isEmpty()) {
      boolean isChanged = false;
      // pick a sentence
      final List<Ngram> sentence = s.pop().ngrams;
      // foreach rule...
      for (final Rules[] rul : rules) {
        for (int i = 0; i < sentence.size(); i++) {
          for (final Rules r : rul) {
            final int size = r.getElements().length;
            if (i + size > sentence.size()) {
              continue;
            }
            final List<Ngram> subsentence = sentence.subList(i, i + size);
            if (r.match(subsentence)) { // check if the ngrams match the rule type
              final List<Ngram> tmp = Lists.newArrayList(sentence);
              tmp.add(i, new Ngram(r.getRet(), subsentence)); // add the generated (new) ngram
              tmp.removeAll(subsentence); // and remove the previous ngrams
              s.push(new Mapping(tmp)); // add the sentence to the search space
              isChanged = true;
              i += size - 1;
              break;
            }
          }
        }
      }
      if (!isChanged) { // if the sentence is unchanged (no transformation can be applied)...
        t.add(new Mapping(sentence)); // it can be added to the translated sentences
      }
    }
    return t;
  }

  private static String getMeasureAsString(final List<Ngram> ngrams, final String par1, final String par2) {
    String res = "";
    String op = null;
    String meas = null;
    for (final Ngram n : ngrams) {
      if (n.children.isEmpty()) {
        if (n.type.equals(Type.OP)) {
          op = n.mde().nameInTable();
        } else {
          meas = n.type.equals(Type.FACT) ? par1 + "*" + par2 : par1 + n.mde().nameInTable() + par2;
        }
      } else {
        if (!res.equals("")) {
          res += ", ";
        }
        res += sqlMeas(n.children);
      }
    }
    if (op != null && meas != null) {
      res += op + meas;
    } else if (op == null) {
      res += "sum" + par1 + meas + par2;
    }
    return res;
  }

  private static String recoursiveMeasure(final List<Ngram> ngrams) {
    return getMeasureAsString(ngrams, " ", "");
  }

  private static String sqlMeas(final List<Ngram> ngrams) {
    return getMeasureAsString(ngrams, "(", ")");
  }

  private static Pair<String, List<Ngram>> getWhere(final List<Ngram> scset) {
    String res = "\nWHERE ";
    List<Ngram> newatt = new ArrayList<>();
    String attr = null;
    String bin = null;
    String val1 = null;
    String not = null;
    for (Ngram n : scset) {
      if (n.type.equals(Type.AND) || n.type.equals(Type.OR)) {
        res += " " + n.mde().nameInTable() + " ";
      } else if (n.type.equals(Type.NOT)) {
        not = n.mde().nameInTable();
      } else if (n.type.equals(Type.ATTR)) {
        attr = n.mde().nameInTable();
      } else if (n.type.equals(Type.BIN)) {
        bin = n.mde().nameInTable();
      } else if (n.type.equals(Type.VAL)) {
        if (bin != null) {
          // ho l'operatore binario
          if (bin.equals("between")) {
            if (val1 == null) {
              val1 = n.mde().nameInTable();
            } else {
              res += attr + " " + bin + " " + val1 + " and " + n.mde().nameInTable();
              attr = null;
              not = null;
              val1 = null;
              bin = null;
            }
          } else {
            if (attr == null && n.mde().refToOtherTable() != -1) {
              // cerca il livello a cui appartiene il membro
              Ngram l = QueryGeneratorChecker.getLevelOfMember(n);
              attr = l.tokens;
              newatt.add(l);
            }
            if (attr != null) {
              res += attr + " " + bin + " " + n.mde().nameInTable();
              attr = null;
              not = null;
              val1 = null;
              bin = null;
            }
          }
        } else if (not != null) {
          // Non ho l'operatore bianrio ma ho il not
          if (attr == null && n.mde().refToOtherTable() != -1) {
            // cerca il livello a cui appartiene il membro
            Ngram l = QueryGeneratorChecker.getLevelOfMember(n);
            attr = l.tokens;
            newatt.add(l);
          }
          if (attr != null) {
            res += not + " " + attr + " = " + n.mde().nameInTable();
            attr = null;
            not = null;
            val1 = null;
            bin = null;
          }
        } else {
          // Non ho né l'operatore bianario né il not => considero implicito =
          if (attr == null && n.mde().refToOtherTable() != -1) {
            // cerca il livello a cui appartiene il membro
            Ngram l = QueryGeneratorChecker.getLevelOfMember(n);
            attr = l.tokens;
            newatt.add(l);
          }
          if (attr != null) {
            res += attr + " = " + n.mde().nameInTable();
            attr = null;
            not = null;
            val1 = null;
            bin = null;
          }
        }
      }
    }
    return Pair.of(res, newatt);
  }

  public static String createQuery(final JSONObject json) throws Exception {
    JSONArray mc = json.getJSONArray(Type.MC.toString());
    final JSONArray gc = json.getJSONArray(Type.GC.toString());
    String select = "SELECT ";
    final Iterator<Object> mcIterator = mc.iterator();
    while (mcIterator.hasNext()) {
      JSONObject mcClause = (JSONObject) mcIterator.next();
      select += mcClause.getString(Type.OP.toString()) + "(" + mcClause.getString(Type.MEA.toString()) + ")" + (mcIterator.hasNext() || !gc.isEmpty() ? ", " : "");
    }
    
    String groupby = gc.isEmpty() ? "" : "\nGROUP BY ";
    final Iterator<Object> gcIterator = gc.iterator();
    final List<Entity> attributes = Lists.newArrayList();
    while (gcIterator.hasNext()) {
      final Entity attr = QueryGeneratorChecker.getLevel(gcIterator.next().toString());
      attributes.add(attr);
      groupby += attr.fullQualifier() + (gcIterator.hasNext() ? ", " : "");
      select += attr.fullQualifier() + (gcIterator.hasNext() ? ", " : "");
    }
    
    JSONArray sc = json.getJSONArray(Type.SC.toString());
    String where = sc.isEmpty() ? "" : "\nWHERE ";
    final Iterator<Object> scIterator = sc.iterator();
    while (scIterator.hasNext()) {
      JSONObject scClause = (JSONObject) scIterator.next();
      final Entity attr = QueryGeneratorChecker.getLevel(scClause.getString(Type.ATTR.toString()));
      attributes.add(attr);
      where += attr.fullQualifier() + scClause.getString(Type.COP.toString()) + scClause.getString(Type.VAL.toString()) + (scIterator.hasNext() ? " AND " : "");
    }
    
    String from = "";
    Set<Integer> tabIns = Sets.newHashSet();
    Pair<Integer, String> ftdet = QueryGeneratorChecker.getFactTable();
    from = "\nFROM " + ftdet.getRight() + " FT ";
    for (Entity mde : attributes) {
      int idT = mde.refToOtherTable();
      if (!tabIns.contains(idT)) {
        Pair<String, String> detTab = QueryGeneratorChecker.getTabDetails(ftdet.getLeft(), idT);
        from += " INNER JOIN " + detTab.getLeft() + " ON " + detTab.getLeft() + "." + detTab.getRight() + " = FT." + detTab.getRight();
        tabIns.add(idT);
      }
    }
    return select + from + where + groupby;
  }

  private static String createQuery(final List<Ngram> ngrams) throws Exception {
    Set<Ngram> attributes = Sets.newLinkedHashSet();
    String select = "";
    String from = "";
    String where = "";
    String groupby = "";

    for (final Ngram ngs : ngrams) {
      if (ngs.type.equals(Type.GC)) {
        Set<Ngram> ga = Ngram.leaves(ngs).stream().map(n -> (Ngram) n).filter(n -> !n.type.equals(Type.GB)).collect(Collectors.toSet());
        groupby = "\nGROUP BY " + ga.stream().map(n -> ((Ngram) n).mde().nameInTable()).reduce((s1, s2) -> s1 + ", " + s2).orElse("");
        attributes.addAll(ga);
        // gbset = s.flatten(ngs.children.stream()).map(n -> (Ngram) n).filter(n -> !n.type.equals(Type.GB)).collect(Collectors.toList());
      } else if (ngs.type.equals(Type.SC)) {
        List<Ngram> scset = Ngram.leaves(ngs).stream().map(n -> (Ngram) n).collect(Collectors.toList());
        // List<Ngram> scset = fillSCset(ngs, checker);
        Pair<String, List<Ngram>> sa = getWhere(scset);
        where = sa.getLeft();
        attributes.addAll(scset.stream().filter(n -> n.type.equals(Type.ATTR)).collect(Collectors.toSet()));
        attributes.addAll(sa.getRight());
      } else if (ngs.type.equals(Type.MC)) {
        select = "SELECT " + sqlMeas(ngs.children);
      }
    }

    Map<Integer, String> tabIns = Maps.newLinkedHashMap();
    if (!select.equals("")) {
      Pair<Integer, String> ftdet = QueryGeneratorChecker.getFactTable();
      from = "\nFROM " + ftdet.getRight() + " FT ";
      int nt = 1;
      for (Ngram gba : attributes) {
        int idT = gba.mde().refToOtherTable();
        String alias;
        if (!tabIns.containsKey(idT)) {
          Pair<String, String> detTab = QueryGeneratorChecker.getTabDetails(ftdet.getLeft(), idT);
          alias = "t" + nt++;
          from += "\nINNER JOIN " + detTab.getLeft() + " " + alias + " ON " + alias + "." + detTab.getRight() + " = FT." + detTab.getRight();
          tabIns.put(idT, alias);
        }
      }
    }

    String query = where + groupby;
    for (Ngram a : attributes) {
      String alias = tabIns.get(a.mde().refToOtherTable());
      query = query.replace(a.mde().nameInTable(), alias + "." + a.mde().nameInTable());
    }

    return select + from + query;
  }

  /**
   * Get the SQL version of the query.
   * @param s mapping
   * @return SQL version of the query
   * @throws Exception in case of error
   */
  public static String getSQLQuery(final Mapping s) throws Exception {
    for (Ngram ngs : s.ngrams) {
      if (ngs.type.equals(Type.GPSJ)) {
        return createQuery(ngs.children);
      }
    }
    return "IT IS NOT POSSIBLE TO CLOSE THE QUERY";
  }

  /**
   * Get a string that separate SC, GBC and MC out of a list of ngrams.
   * @param ngrams list of ngrams
   * @return string in the format MC\GBC\SC
   */
  public static String getPartOfResult(final List<Ngram> ngrams) {
    String select = "";
    String gb = "";
    String where = "";
    for (Ngram nch : ngrams) {
      if (nch.type == Type.GPSJ) {
        return getPartOfResult(nch.children);
      } else {
        if (nch.type == Type.SC) {
          where = Ngram.leaves(nch).stream().map(n -> ((Ngram) n).mde().nameInTable()).reduce((s1, s2) -> s1 + " " + s2).get();
        } else if (nch.type == Type.GC) {
          gb = Ngram.leaves(nch).stream().filter(n -> ((Ngram) n).type != Type.GB).map(n -> ((Ngram) n).mde().nameInTable()).reduce((s1, s2) -> s1 + ", " + s2).get();
        } else if (nch.type == Type.MC) {
          select = recoursiveMeasure(nch.children);
        }
      }
    }
    return select + "\\" + gb + "\\" + where;
  }
}
