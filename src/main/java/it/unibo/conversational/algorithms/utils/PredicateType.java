package it.unibo.conversational.algorithms.utils;

/**
 * Data type for query items.
 */
public enum PredicateType {
  /** Select statement. */
  SELECT,
  /** >=, <=, =, ... */
  PREDICATE, 
  /** avg, sum, min, max, ... */
  GROUPBYOPERATOR, 
  /** by. */
  GROUPBYTERM,
  /** ??? */
  BOOLEANOPEAROR,
  /** where. */
  SELECTIONTERM,
  /** count, count distinct. */
  COUNTOPERATOR
}