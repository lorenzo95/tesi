package it.unibo.conversational.algorithms.utils;

/** 
 * Data type handled by our system.
 */
public enum DataType {
  /** A number. */
  NUMERIC,
  /** A string. */
  STRING,
  /** A date. */
  DATE,
  /** A MC / SC / GBC clause. */
  OTHER
}
