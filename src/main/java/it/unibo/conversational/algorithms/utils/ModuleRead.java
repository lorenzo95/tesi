package it.unibo.conversational.algorithms.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Set;

import com.google.common.collect.Sets;

import it.unibo.conversational.Utils;
import it.unibo.conversational.algorithms.Mapper;

public class ModuleRead {
	/**
	   * Get credentials from resources/credentials.txt.
	   * 
	   * File must contain:
	   * IP\n
	   * PORT\n
	   * USENAME\n
	   * PASSWORD\n
	   * FACTDB\n
	   * FACTTABLE\n
	   * METADB
	   * @return array with credentials
	   */
	  public static String[] credentialsFromFile() {
	    try (
	        BufferedReader b = new BufferedReader(new InputStreamReader(Utils.getUtils().getClass().getClassLoader().getResourceAsStream("credentials.txt")))
	    ) {
	      String readLine = "";
	      final String[] credentials = new String[8];
	      int i = 0;
	      while ((readLine = b.readLine()) != null) {
	        credentials[i++] = readLine;
	      }
	      b.close();
	      return credentials;
	    } catch (Exception e) {
	      return null;
	    }
	  }
	  
	  /**
	   * Read the list of stopwords from file.
	   * @return the list of stopwords
	   * @throws Exception in case of error
	   */
	  public static Set<String> readStopWord() throws Exception {
	    final Set<String> stopWord = Sets.newLinkedHashSet();
	    final BufferedReader br = new BufferedReader(new InputStreamReader(Mapper.getMapper().getClass().getClassLoader().getResourceAsStream("stopwords.txt")));
	    String st;
	    while ((st = br.readLine()) != null) {
	      stopWord.add(st);
	    }
	    return stopWord;
	  }
}
