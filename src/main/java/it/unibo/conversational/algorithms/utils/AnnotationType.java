package it.unibo.conversational.algorithms.utils;

/** Type of errors and ambiguities. */
public enum AnnotationType {
  /** Operator cannot be applied to the given measure. */
  MDMV,
  /** Group by on descriptive attribute. TODO: not implemented for now. */
  MDMG,
  /** Several measure can be inferred for the given measure. */
  MA,
  /** Several attributes can be inferred for the given member. */
  AA,
  /** Dom(L) != Dom(Member). */
  AVM,
  /** Unparsed. TODO: not implemented for now. */
  UP
}