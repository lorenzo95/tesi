package it.unibo.conversational.algorithms.utils;

/** Table types. */
public enum TableTypes {
  /** Fact table. */
  FT,
  /** Dimension table. */
  DT
}
