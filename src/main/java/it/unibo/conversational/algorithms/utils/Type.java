package it.unibo.conversational.algorithms.utils;

/**
 * Rules type.
 */
public enum Type {
  /** Measure clause. */
  MC,
  /** Measure. */
  MEA,
  /** Fact name. */
  FACT,
  /** Hierarchy. */
  H,
  /** Measure aggregation. */
  OP,
  /** Group by `by`. */
  GB,
  /** Group by `where`. */
  WHR,
  /** Group by clause. */
  GC,
  /** Level. */
  ATTR,
  /** Comparison operator. */
  COP,
  /** Selection clause. */
  SC,
  /** Value. */
  VAL,
  /** Between operator. */
  BETWEEN,
  /** Logical and. */
  AND,
  /** Logical or. */
  OR,
  /** Logical not. */
  NOT,
  /** `Select`. */
  SELECT,
  /** Container for not mapped tokens. */
  BIN,
  /** Query. */
  GPSJ,
  /** Count, count distinct. */
  COUNT,
  /** Dummy container for Servlet purpose. */
  FOO;
}
