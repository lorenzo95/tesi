package it.unibo.conversational.algorithms.utils;

import java.util.List;

import it.unibo.conversational.datatypes.Ngram;

/**
 * Type of rules used for the parsing.
 */
public enum Rules {
/** MC ::= OP MEA. */
M1(Type.MC, Type.OP, Type.MEA), // M1 is more generic than M3
/** MC ::= MEA OP. */
M2(Type.MC, Type.MEA, Type.OP), // M2 is more generic than M3
/** MC ::= Count fact. */
M5(Type.MC, Type.COUNT, Type.FACT),
/** MC ::= MC MC. */
M4(Type.MC, Type.MC, Type.MC),
/** MEA ::= OP MEA. */
M3(Type.MC, Type.MEA),
// M5(Type.MC, false, Type.COUNT, Type.FACT),//

/** GBC ::= BY LEV. */
G1(Type.GC, Type.GB, Type.ATTR), //
/** GBC ::= GBC LEV. */
G2(Type.GC, Type.GC, Type.ATTR), //
// /** GBC ::= LEV. */
// G3(Type.GC, false, Type.ATTR), // do not apply G3 before S*, otherwise it won't recognize levels for SC

/** SC ::= LEV COP VAL. */
S1(Type.SC, Type.ATTR, Type.COP, Type.VAL),
/** SC ::= VAL COP LEV. */
S2(Type.SC, Type.VAL, Type.COP, Type.ATTR),
/** SC ::= LEV VAL. */
S3(Type.SC, Type.ATTR, Type.VAL),
/** SC ::= VAL LEV. */
S4(Type.SC, Type.VAL, Type.ATTR),
/** SC ::= SC AND SC. */
S5(Type.SC, Type.SC, Type.AND, Type.SC),
/** SC ::= SC OR SC. */
S6(Type.SC, Type.SC, Type.OR, Type.SC),
/** SC ::= VAL. */
S8(Type.WHR, Type.SC),
/** SC ::= VAL. */
S7(Type.SC, Type.VAL),

/** Q ::= GBC SC MC. */
Q1(Type.GPSJ, Type.GC, Type.SC, Type.MC),
/** Q ::= GBC SC MC. */
Q2(Type.GPSJ, Type.GC, Type.MC, Type.SC),
/** Q ::= GBC SC MC. */
Q3(Type.GPSJ, Type.SC, Type.GC, Type.MC),
/** Q ::= GBC SC MC. */
Q4(Type.GPSJ, Type.SC, Type.MC, Type.GC),
/** Q ::= GBC SC MC. */
Q5(Type.GPSJ, Type.MC, Type.GC, Type.SC),
/** Q ::= GBC SC MC. */
Q6(Type.GPSJ, Type.MC, Type.SC, Type.GC),
/** Q ::= GBC MC. */
Q7(Type.GPSJ, Type.MC, Type.GC),
/** Q ::= GBC MC. */
Q8(Type.GPSJ, Type.GC, Type.MC),
/** Q ::= SC MC. */
Q9(Type.GPSJ, Type.MC, Type.SC),
/** Q ::= SC MC. */
Q10(Type.GPSJ, Type.SC, Type.MC),
/** Q ::= MC. */
Q11(Type.GPSJ, Type.MC);

private final Type[] elements;
private final Type ret;

/**
 * Create a rule.
 * @param ret return type
 * @param acceptPermutation whether the rule accept permutation
 * @param elements list of elements
 */
Rules(final Type ret, final Type... elements) {
  this.ret = ret;
  this.elements = elements;
}

/**
 * Check whether a list of ngrams match this rule.
 * @param ngrams list of ngrams
 * @return true if the list of ngrams match this rule
 */
public boolean match(final List<Ngram> ngrams) {
  boolean found = ngrams.size() == elements.length;
  for (int i = 0; i < ngrams.size() && found; i++) {
    found = found && ngrams.get(i).type.equals(elements[i]);
  }
  return found;
}

public Type getRet() {
	return ret;
}

public Type[] getElements() {
	return elements;
}
}