package it.unibo.conversational.algorithms.utils;

import java.util.Set;

import com.google.common.collect.Sets;

public final class TabTable {
	/****************************/
	/** Table tabTABLE. */
	public static final String tabTABLE = "table";
	/** Table tabRELATIONSHIP. */
	public static final String tabRELATIONSHIP = "relationship";
	/** Table tabCOLUMN. */
	public static final String tabCOLUMN = "column";
	/** Table tabDATABASE. */
	public static final String tabDATABASE = "database";
	/** Table tabFACT. */
	public static final String tabFACT = "fact";
	/** Table tabHiF. */
	public static final String tabHiF = "hierarchy_in_fact";
	/** Table tabHIERARCHY. */
	public static final String tabHIERARCHY = "hierarchy";
	/** Table tabLEVEL. */
	public static final String tabLEVEL = "level";
	/** Table tabMEMBER. */
	public static final String tabMEMBER = "member";
	/** Table tabMEASURE. */
	public static final String tabMEASURE = "measure";
	/** Table tabGROUPBYOPERATOR. */
	public static final String tabGROUPBYOPERATOR = "groupbyoperator";
	/** Table tabSYNONYM. */
	public static final String tabSYNONYM = "synonym";
	/** Table tabGRBYOPMEASURE. */
	public static final String tabGRBYOPMEASURE = "groupbyoperator_of_measure";
	/** Table tabLANGUAGEPREDICATE. */
	public static final String tabLANGUAGEPREDICATE = "language_predicate";
	/** Table tabLEVELROLLUP. */
	public static final String tabLEVELROLLUP = "level_rollup";
	/** Table tabQuery. */
	public static final String tabQuery = "queries";
	/** List of table containing the synonyms column. */
	public static final Set<String> tabsWithSyns = //
      Sets.newHashSet(tabGROUPBYOPERATOR, tabFACT, tabLEVEL, tabMEASURE, tabMEMBER, tabLANGUAGEPREDICATE);

	public static final String colRELTAB1 = "table1";
	public static final String colRELTAB2 = "table2";
	public static final String colCOLISKEY = "isKey";
	public static final String colDBIP = "IPaddress";
	public static final String colDBPORT = "port";
	public static final String colLEVELCARD = "cardinality";
	public static final String colLEVELMIN = "min";
	public static final String colLEVELMAX = "max";
	public static final String colLEVELAVG = "avg";
	public static final String colLEVELMINDATE = "mindate";
	public static final String colLEVELMAXDATE = "maxdate";
	public static final String colLEVELRUSTART = "start";
	public static final String colLEVELRUTO = "level_to";
	public static final String colGROUPBYOPNAME = "operator";
	public static final String colSYNTERM = "term";
	public static final String colQueryID = "id";
	public static final String colQueryText = "query";
	public static final String colQueryGBset = "gc";
	public static final String colQuerySelClause = "sc";
	public static final String colQueryMeasClause = "mc";
	public static final String colQueryGPSJ = "gpsj";
	/****************************/
	
	/****************************/
	/** Default THR_MEMBER. */
	public static final double THR_MEMBER = 0.8;
	/** Default THR_META. */
	public static final double THR_META = 0.4;
	/** Default N_SYNMEMBER. */
	public static final int N_SYNMEMBER = 1;
	/** Default N_SYNMETA. */
	public static final int N_SYNMETA = 5;
	/** Default THR_COVERAGE. */
	public static final double THR_COVERAGE = 0.7;
	/** Default THR_NGRAMDIST. */
	public static final int THR_NGRAMDIST = 3;
	/** Default NGRAM_SIZE. */
	public static final int NGRAM_SIZE = 3;
	/** Default NGRAMSYNTHR. */
	public static final double NGRAMSYNTHR = 0.9;
	/** Default K. */
	public static final int K = 5;
	
	//	  Test parameters (prima private adesso public)
	public static final int N_RUNS = 2;
	public static final int[] N_SYNMETAS = new int[]{5, 3, 1};
	public static final double[] THR_METAS = new double[]{0.4, 0.5, 0.6};
	public static final double[] THR_MEMBERS = new double[]{0.8, 0.9};
	/****************************/
}
