package esempi;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;

public class Server extends AbstractVerticle {
	
	public static String createPage() {
		return "<p>Hello from Vert.x!</p>"
				+ "<button>clicca</button>";
	}
	
	@Override
	public void start() {
	    vertx.createHttpServer().requestHandler(req -> {
	      req.response()
	        .putHeader("content-type", "text/html")
	        .end(createPage());
	    }).listen(8080);
	  }	

	public static void main(String[] args) {		
		Vertx.vertx().deployVerticle(new Server());
	}
}
