package esempi;

import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DateServlet extends HttpServlet { 
	public static final long serialVersionUID = 23396864;
	
	public DateServlet() { }

	public Date getDate() { return new Date(); }
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, java.io.IOException { 
		try { resp.setContentType("text/plain"); 
		PrintWriter out = resp.getWriter(); out.println(getDate().toString()); } catch (Exception e) { e.printStackTrace(); } 
	}
	
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, java.io.IOException { 
		doGet(req, resp); 
	} 
}