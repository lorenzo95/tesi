package esempi;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;

public class AudioTest {
	public static TargetDataLine microphone;
	public static AudioFormat format;
	public static AudioInputStream audioInputStream;
	public static SourceDataLine sourceDataLine;
	public static ByteArrayOutputStream out;
	
	public static int BYTES_READ = 100000;
	public static int TEMP_READ = 10000;
	
	public static int CHUNK_SIZE = 1024;
	public static int FACTOR_DIV = 5;
	
	public static byte[] data;
	public static byte[] audioData;
	public static byte[] tempBuffer;
	
	public static void record() {
		try {
			microphone = AudioSystem.getTargetDataLine(format);
	
	        DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
	        microphone = (TargetDataLine) AudioSystem.getLine(info);
	        microphone.open(format);
	
	        out = new ByteArrayOutputStream();
	        data = new byte[microphone.getBufferSize() / FACTOR_DIV];
	        microphone.start();
	
	        int bytesRead = 0;
	        try {
	            while (bytesRead < BYTES_READ) {
	                int numBytesRead = microphone.read(data, 0, CHUNK_SIZE);
	                bytesRead += numBytesRead;
	                out.write(data, 0, numBytesRead);
	            }
	        } catch (Exception e) {
	        	System.out.println("Exception exc".concat(e.getMessage()));
	        }
		} catch (LineUnavailableException e) {
			System.out.println("LineUnavailableException exc = ".concat(e.getMessage()));
		}
	}
	
	public static void reproduceAudio() {
		try {
			audioData = out.toByteArray();
	        InputStream byteArrayInputStream = new ByteArrayInputStream(audioData);
	        audioInputStream = new AudioInputStream(byteArrayInputStream, format, audioData.length / format.getFrameSize());
	        
	        DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, format);
	        sourceDataLine = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
	        sourceDataLine.open(format);
	        sourceDataLine.start();
	        
	        int cnt = 0;
	        tempBuffer = new byte[TEMP_READ];
	        try {
	            while ((cnt = audioInputStream.read(tempBuffer, 0,tempBuffer.length)) != -1) {
	                if (cnt > 0) {
	                    // Write data to the internal buffer of
	                    // the data line where it will be delivered to the speaker.
	                    sourceDataLine.write(tempBuffer, 0, cnt);
	                }// end if
	            }
	        } catch (IOException e) {
	        	System.out.println("Eccezione input = ".concat(e.getMessage()));
	        }

	        sourceDataLine.drain();
	        sourceDataLine.close();
	        microphone.close();
		} catch (LineUnavailableException e) {
			System.out.println("LineUnavailableException exc = ".concat(e.getMessage()));
		}
	}
	
    public static void main(String[] args) {
           format = new AudioFormat(8000.0f, 16, 1, true, true);
    	   record();
    	   reproduceAudio();

    	   System.out.println("Elem to String ".concat(out.toString()));
    	   String string = new String(data, StandardCharsets.UTF_8);    	
    	   System.out.println("string ".concat(string));
    }
}
