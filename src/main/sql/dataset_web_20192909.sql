DROP TABLE IF EXISTS `queries`;
CREATE TABLE `queries` (
  `query_id` int(11) NOT NULL AUTO_INCREMENT,
  `query` text NOT NULL,
  `gbset` text,
  `selectionpredicate` text,
  `measureclause` text,
  PRIMARY KEY (`query_id`)
) ENGINE=InnoDB AUTO_INCREMENT=294 DEFAULT CHARSET=utf8mb4;
INSERT INTO `queries` VALUES
  (1,'sum sales by customer year greater 2019','customer_id','the_year >= 2019','sum unit_sales'),
  (3,'by customer year','customer_id, the_year','',''),
  (4,'sum unit sales avg store sales','','','sum unit_sales, avg store_sales'),
  (6,'sum unit sales','','','sum unit_sales'),
  (7,'by customer and year','customer_id, the_year','',''),
  (8,'by customer and store','customer_id, store_id','',''),
  (9,'by cutomer and store','customer_id, store_id','',''),
  (10,'by cutomer and year','customer_id, the_year','',''),
  (11,'by cutomer and also year','customer_id, the_year','',''),
  (12,'by customer and also year','customer_id, the_year','',''),
  (13,'year greater 2019 and city = novato','','the_year >= 2019 and city = \"Novato\"\"\"',''),
  (14,'by customer year greater 2019 and city = novato','customer_id','the_year >= 2019 and city = \"Novato\"\"\"',''),
  (15,'by customer year greater 2019','customer_id','the_year >= 2019',''),
  (17,'by customer year greater 2019 city = Novato','','',''),
  (18,'sum sales avg store sales','','','sum unit_sales, avg store_sales'),
  (19,'unit sales','','',''),
  (20,'unit sold','','',''),
  (21,'sum sales avg sales','','',''),
  (22,'sum sales avg cost','','','sum unit_sales, avg store_cost'),
  (23,'sum sales by customer year greater 2018 and city = Cesena','customer_id','the_year >= 2018','sum unit_sales'),
  (24,'grouping by customer return the sum of sales where year greater 2018','customer_id','the_year >= 2018','sum unit_sales'),
  (25,'grouping by customer where year greater 2018','customer_id','the_year >= 2018',''),
  (26,'grouping by customer return sum of sales','customer_id','','sum unit_sales'),
  (27,'year greater 2018 and city = Novato','','the_year >= 2018 and city = \"Novato\"\"\"',''),
  (29,'year greater 1900 and city = Novato','','the_year >= 1900 and city = \"Novato\"\"\"',''),
  (31,'sum unit','','','sum unit_sales'),
  (33,'sum of sales','','','sum unit_sales'),
  (34,'sale sum','','','sum unit_sales'),
  (35,'grouping by customer return the sales sum where year greater 2018','customer_id','the_year >= 2018','sum unit_sales'),
  (37,'sum sales and avg cost','','','sum unit_sales, avg store_cost'),
  (38,'show sum sales and avg cost','','','sum unit_sales, avg store_cost'),
  (39,'sum sales by customer year region','customer_id, the_year','','sum unit_sales'),
  (40,'sum sales by customer region year ','customer_id, the_year','','sum unit_sales'),
  (41,'sum min sales by customer region year ','','',''),
  (42,'year greater 2018 and city = Cesena','','the_year >= 2018',''),
  (43,'sum sales by customer product_name = Club Chocolate Milk','customer_id','product_name = \"Club Chocolate Milk\"\"\"','sum unit_sales'),
  (53,'sum sales','','','sum unit_sales'),
  (66,'sum sales by customer','customer_id','','sum unit_sales'),
  (70,'sum sales customer','','',''),
  (71,'sum sales customer year','','',''),
  (72,'by customer','customer_id','',''),
  (73,'sum sales by customer year greater 1900','customer_id','the_year >= 1900','sum unit_sales'),
  (74,'sum sale','','','sum unit_sales'),
  (75,'sum unit sales by customer year greater 1900','customer_id','the_year >= 1900','sum unit_sales'),
  (76,'sum unit sales by customer product_name = Club Chocolate Milk','customer_id','product_name = \"Club Chocolate Milk\"\"\"','sum unit_sales'),
  (77,'sum unit sales by customer product name = Club Chocolate Milk','customer_id','product_name = \"Club Chocolate Milk\"\"\"','sum unit_sales'),
  (78,'sum unit sales year greater 1900','','the_year >= 1900','sum unit_sales'),
  (79,'sum unit avg cost','','','sum unit_sales, avg store_cost'),
  (80,'sum unit sales by customer where year greater 1900','customer_id','the_year >= 1900','sum unit_sales'),
  (81,'sum unit by customer where year greater 1900','customer_id','the_year >= 1900','sum unit_sales'),
  (82,'sum sales by customer year greater 2018 and city = Novato','customer_id','the_year >= 2018 and city = \"Novato\"\"\"','sum unit_sales'),
  (83,'sum unit by customer year greater 2018 and city = Novato','customer_id','the_year >= 2018 and city = \"Novato\"\"\"','sum unit_sales'),
  (84,'sum unit by customer year greater 2018 or product name = Club Chocolate Milk','customer_id','the_year >= 2018 or product_name = \"Club Chocolate   Milk\"\"\"','sum unit_sales'),
  (85,'sum unit by customer year greater 2018 and city = Cesena','customer_id','the_year >= 2018','sum unit_sales'),
  (86,'sum unit by customer year greater 2018 and city = Rimini','customer_id','the_year >= 2018','sum unit_sales'),
  (87,'sum unit and avg cost by customer year greater 2018 and city = Cesena','customer_id','the_year >= 2018','sum unit_sales, avg store_cost'),
  (88,'sum unit and avg cost ','','','sum unit_sales, avg store_cost'),
  (89,'sum unit by customer year','customer_id, the_year','','sum unit_sales'),
  (90,'sum unit and avg cost by customer year','customer_id, the_year','','sum unit_sales, avg store_cost'),
  (91,'sum unit and avg cost by customer and city = Cesena','customer_id','','sum unit_sales, avg store_cost'),
  (92,'sum unit and avg cost by customer year greater 2018 and city = Novato','customer_id','the_year >= 2018 and city = \"Novato\"\"\"','sum unit_sales, avg   store_cost'),
  (93,'sum unit and avg cost by customer city = Cesena and year greater 2018','customer_id','the_year >= 2018','sum unit_sales, avg store_cost'),
  (95,'sum unit city = Rimini','customer_id','the_year >= 2018','sum unit_sales'),
  (96,'sum unit and avg cost by customer year greater 2018 and city = Cesena','','','sum unit_sales, avg store_cost'),
  (98,'sum unit by customer year greater 2018','customer_id','the_year >= 2018','sum unit_sales'),
  (100,'sum unit sales and avg cost by customer year greater 1900','customer_id','the_year >= 1900','sum unit_sales, avg store_cost'),
  (101,'sum units by customer','customer_id','','sum unit_sales'),
  (102,'sum units and avg cost by customer year greater 1900','customer_id','the_year >= 1900','sum unit_sales, avg store_cost'),
  (103,'sum units by customer year greater 1900','customer_id','the_year >= 1900','sum unit_sales'),
  (104,'sum units by customer for year greater than 1990','customer_id','the_year >= 1990','sum unit_sales'),
  (105,'sum unit and avg cost sales by customer year greater 1900','customer_id','the_year >= 1900','sum unit_sales, avg store_cost'),
  (106,'sum unit sales and avg cost','','','sum unit_sales, avg store_cost'),
  (107,'sum unit sales and avg cost by customer','customer_id','','sum unit_sales, avg store_cost'),
  (108,'sum unit sales an avg cost','','','sum unit_sales, avg store_cost'),
  (109,'sum unit sales and avg cost by customer and year','customer_id, the_year','','sum unit_sales, avg store_cost'),
  (110,'sum unit sales and avg cost by customer and state for year greater 1900','customer_id, store_state','the_year >= 1900','sum unit_sales, avg store_cost'),
  (111,'sum unit sales and avg cost by customer and state for year greater 1900 and product = Club Chocolate Milk','','',''),
  (112,'avg cost for year greater 1900 and product = Club Chocolate Milk','','the_year >= 1900 and product_name = \"Club Chocolate Milk\"\"\"','avg store_cost'),
  (113,'avg cost for year greater 1900 and product name = Club Chocolate Milk','','the_year >= 1900 and product_name = \"Club Chocolate Milk\"\"\"','avg   store_cost'),
  (114,'year greater 1900 and product name = Club Chocolate Milk','','the_year >= 1900 and product_name = \"Club Chocolate Milk\"\"\"',''),
  (115,'sum unit for year greater 1900 and product name = Club Chocolate Milk','','the_year >= 1900 and product_name = \"Club Chocolate Milk\"\"\"','sum   unit_sales'),
  (116,'sum unit sales by customer year greater 1900 and city = Novato','customer_id','the_year >= 1900 and city = Novato','sum unit_sales'),
  (117,'sum unit sales and avg cost by customer year greater 1900 and city = Novato','customer_id','the_year >= 1900 and city = \"Novato\"\"\"','sum unit_sales,   avg store_cost'),
  (118,'sum unit and avg cost year greater 2018 and city = Cesena','','the_year >= 2018','sum unit_sales, avg store_cost'),
  (119,'sum unit and avg cost by customer and year','customer_id, the_year','','sum unit_sales, avg store_cost'),
  (120,'sum unit year between 1900 and 1950','','the_year between 1900 1950','sum unit_sales'),
  (121,'sum unit year between 1900 1950','','the_year between 1900 1950','sum unit_sales'),
  (122,'grouping by customer return sum of sales','customer_id','','sum unit_sales'),
  (123,'show sum unit sold grouped by quarter','quarter','','sum unit_sales'),
  (124,'sum unit by cutomer','customer_id','','sum unit_sales'),
  (127,'get the minimun cost for store for product of Denny','','brand_name = Denny','min store_cost'),
  (128,'get the minimun cost for store for product of brand Denny','','brand_name = Denny','min store_cost'),
  (129,'show the max sales grouped by customer gender','gender','','max unit_sales '),
  (130,'numer of unit sold by gender','gender','','sum unit_sales'),
  (131,'in which quarter there is the max number of unit sold','quarter','','max unit_sales'),
  (132,'number of unit sold per store','store_id','','sum unit_sales'),
  (133,'in which quarter there is the max store sales','quarter','','max store_sales'),
  (134,'medium cost by promotion','promotion_id','','avg store_cost'),
  (135,'sum cost','','',''),
  (136,'sum of unit sold for each customer','customer_id','','sum unit_sales'),
  (137,'sum of unit sold based on media_type','media_type','','sum unit_sales'),
  (138,'average unit sold by city and quarter','city, quarter','','avg unit_sales'),
  (139,'sum of unit sold for each promotion','promotion_id','','sum unit_sales'),
  (140,'show me the sum of sales for each product','product_id','','sum store_sales'),
  (141,'show me the unit sold by customer for product of category Fruit','customer_id','product_category = Fruit','sum unit_sales'),
  (142,'for promotion Daily Paper show the sum of store sales by quarter','quarter','media_type = \"Daily Paper\"\"\"','sum store_sales'),
  (143,'average unit sold by customer','customer_id','','avg unit_sales'),
  (144,'sum of store sales by month of each store','the_month, store_id','','sum store_sales'),
  (145,'sales fact','','',''),
  (146,'count store sales','','',''),
  (147,'count sales fact','','',''),
  (148,' sum unit by customer year greater 2018 and city = Cesena','','',''),
  (149,'sum unit sales year greater 1900 by customer ','','',''),
  (150,'sum unit sales  by customer  year greater 1900','','',''),
  (151,'sum unit sales  year greater 1900 by customer','','',''),
  (153,'sum unit sales year greater 1950','','',''),
  (154,'sum unit sales by customer year greater 1950','','',''),
  (155,'sum unit year greater 1900','','',''),
  (156,'sum unit sales for 1900','','',''),
  (157,'sum unit sales for year 1900','','',''),
  (158,'sum unit sale for year 1900','','',''),
  (159,'sum unit sale for 1900','','',''),
  (160,'sum unit sale by customer','','',''),
  (161,'sum unit sale','','',''),
  (162,'sum uni sales','','',''),
  (163,'sum unit for 1900','','',''),
  (164,'sum unit by customer','','',''),
  (165,'salem salem','','',''),
  (166,'salem gum','','',''),
  (167,'sum unit for Club Chocolate Milk','','',''),
  (168,'sum unit for Novato','','',''),
  (169,'sum unit for city Novato','','',''),
  (170,'sum unit for city Novato and year 1900','','',''),
  (171,'sum unit by customer for 1900',NULL,NULL,NULL),
  (172,'unit\'s sum','','','sum unit_sales'),
  (173,'unit\'s sum for year between 1900 and 1950',NULL,NULL,NULL),
  (174,'year between 1900 and 1950',NULL,NULL,NULL),
  (175,'year = 1900 and city = Novato',NULL,NULL,NULL),
  (176,'sum unit for year = 1900',NULL,NULL,NULL),
  (177,'city = Novato',NULL,NULL,NULL),
  (178,'sum unit for year = 1900 and city = Novato',NULL,NULL,NULL),
  (179,'sum unit for year not 1900',NULL,NULL,NULL),
  (180,'year not 1900',NULL,NULL,NULL),
  (181,'sum of unit sold',NULL,NULL,NULL),
  (182,'Average number of units purchased by each customer',NULL,NULL,NULL),
  (183,'number of unit sold by gender',NULL,NULL,NULL),
  (184,'number of unit sold for each customer',NULL,NULL,NULL),
  (185,'Units sold based on the media type of the promotion',NULL,NULL,NULL),
  (186,'Geographical analysis of sales (by city and period)',NULL,NULL,NULL),
  (187,'Sales related to each promotion',NULL,NULL,NULL),
  (188,'show the sum of sales for each product',NULL,NULL,NULL),
  (189,'Performance of monthly sales for products sold with the Daily Papper promotion',NULL,NULL,NULL),
  (190,'Sales made by each store',NULL,NULL,NULL),
  (191,'Analysis of the earnings per month of each store',NULL,NULL,NULL),
  (192,'View monthly earnings by product category',NULL,NULL,NULL),
  (193,'Geographical analysis of payments by city and quarter',NULL,NULL,NULL),
  (194,'Number of units sold on average to each customer',NULL,NULL,NULL),
  (195,'Monthly costs of each store',NULL,NULL,NULL),
  (196,'Revenues by product category and by store in February',NULL,NULL,NULL),
  (197,'How much did each store earn in August?',NULL,NULL,NULL),
  (198,'Analysis of purchases by promotion over several months',NULL,NULL,NULL),
  (199,'Average cost of products for each quarter',NULL,NULL,NULL),
  (200,'Sales analysis based on the product department',NULL,NULL,NULL),
  (201,'Average cost of products per quarter and product subcategory',NULL,NULL,NULL),
  (202,'Analyze the costs by comparing the same month in different years',NULL,NULL,NULL),
  (203,'Geographical analysis of sales (by city and month)',NULL,NULL,NULL),
  (204,'Sales performance in the various months for products in the Candy category',NULL,NULL,NULL),
  (205,'Analyze the average number of units sold based on the cost of the promotion',NULL,NULL,NULL),
  (206,'How many times have the products been bought by month, product category and store',NULL,NULL,NULL),
  (207,'View products by media type of the promotion and by store in the month of September',NULL,NULL,NULL),
  (208,'Performance of monthly sales for products sold with the Daily Paper promotion',NULL,NULL,NULL),
  (209,'number of unit sold by gender and the_year',NULL,NULL,NULL),
  (210,'number of unit sold by gender and year',NULL,NULL,NULL),
  (211,'sum sales by customer year greater 2018',NULL,NULL,NULL),
  (212,'sum unit sold by customer year greater 2018',NULL,NULL,NULL),
  (213,'Units sold based on the media type',NULL,NULL,NULL),
  (214,'show me the sum of store sales for the month of January grouped by store city',NULL,NULL,NULL),
  (215,'Sales by Customer and Month',NULL,NULL,NULL),
  (216,'medium store cost by promotion',NULL,NULL,NULL),
  (217,'avg store_cost by promotion_id',NULL,NULL,NULL),
  (218,'average store_cost by promotion_id',NULL,NULL,NULL),
  (219,'sum store_cost by promotion_id',NULL,NULL,NULL),
  (220,'Average units',NULL,NULL,NULL),
  (221,'Average unit sales',NULL,NULL,NULL),
  (222,'Minimum unit sales',NULL,NULL,NULL),
  (223,'Maximum unit sales',NULL,NULL,NULL),
  (224,'medium store cost by promotion_id',NULL,NULL,NULL),
  (225,'Units sold by media type of the promotion',NULL,NULL,NULL),
  (226,'unit sold by customer year 2018',NULL,NULL,NULL),
  (227,'unit by customer year 2018',NULL,NULL,NULL),
  (228,'by gender the_year',NULL,NULL,NULL),
  (229,'by gender year',NULL,NULL,NULL),
  (230,'sum unit sales in 2019',NULL,NULL,NULL),
  (231,'sum store cost by promotion id',NULL,NULL,NULL),
  (232,'average store cost by promotion id',NULL,NULL,NULL),
  (233,'medium store cost by promotion id',NULL,NULL,NULL),
  (234,'sum unit sales for 2019',NULL,NULL,NULL),
  (235,'sum unit sales by year',NULL,NULL,NULL),
  (236,'sum unit sales for year',NULL,NULL,NULL),
  (237,'media type',NULL,NULL,NULL),
  (238,'sum unit sales in year',NULL,NULL,NULL),
  (239,'sum unit sales by customer and promotion',NULL,NULL,NULL),
  (240,'sum unit sales by customer and promotion and product',NULL,NULL,NULL),
  (241,'unit sales by customer and promotion and product',NULL,NULL,NULL),
  (242,'unit sales where year is 2019 by customer and promotion and product',NULL,NULL,NULL),
  (243,'sum unit sales for year 2019 and group by customer promotion and product',NULL,NULL,NULL),
  (244,'unit sales year 2019 by customer and promotion and product',NULL,NULL,NULL),
  (245,'sum unit sales by customer and product year is 2019','customer_id, product_id','','sum unit_sales'),
  (246,'sum units saless','','',''),
  (247,'sum unit sales by customer','customer_id','','sum unit_sales'),
  (248,'sum units saless','','','sum unit_sales'),
  (249,'sum units saless','','','sum unit_sales'),
  (250,'sum units saless','','','sum unit_sales'),
  (251,'sum units saless','','','sum unit_sales'),
  (252,'sum units saless','','','sum unit_sales'),
  (253,'sum units saless','','','sum unit_sales'),
  (254,'sum unit sales by customer and year','customer_id, the_year','','sum unit_sales'),
  (255,'units saless by customer','','',''),
  (256,'units saless by customer unit sales','','',''),
  (257,'sum unit sales where customer is Salem and year is 2019','','',''),
  (258,'sum unit sales where customer is Salem and year is 2019 product is coca','','',''),
  (259,'sum unit sales where customer is Salem and year is 2019','','',''),
  (260,'sum unit sales where customer is Salem and year is 2019 or year is 2020','','',''),
  (261,'sum unit sales where customer is Salem and year is 2019 product is club chocolate milk','','',''),
  (262,'sum unit sales average','','',''),
  (263,'sum unit sales average','','',''),
  (264,'sum unit sales where customer is Salem and year is 2019 or year is 2020','','',''),
  (265,'sum unit sales by customer where year is 2019','','',''),
  (266,'sum unit sales in 2019','','',''),
  (267,'sum unit sales for year','','',''),
  (268,'average unit sales sales by product category','','',''),
  (269,'average unit sales by product category for product club chocolate milk','','',''),
  (270,'sum sales','','',''),
  (271,'sales by month, product and year unit sales','','',''),
  (272,'sales by month, product and year unit sales','','',''),
  (273,'unit sales by month, product and year unit sales','','',''),
  (274,'unit sales for subcategory','','',''),
  (275,'unit sales for Salem','','',''),
  (276,'unit sales for customer Salem','','',''),
  (277,'unit sales for product club chockolate milk','','',''),
  (278,'unit sales where product club chockolate milk','','',''),
  (279,'unit sales where product is club chockolate milk','','',''),
  (280,'unit sales where product is club chockolate milk','','',''),
  (281,'unit sales where product is club chockolate milk','','',''),
  (282,'unit sales where product is club chockolate milk','','',''),
  (283,'unit sales for club chockolate milk','','',''),
  (284,'unit sales for club chockolate milk','','',''),
  (285,'Tell me the sum of unit sold grouped by customer','','',''),
  (286,'Tell me the sum of unit sold grouped by customer','','',''),
  (287,'Tell me the sum of unit sold grouped by customer','','',''),
  (288,'Tell me the sum of unit sold grouped by customer','','',''),
  (289,'Tell me the sum of unit sold grouped by customer','','',''),
  (290,'Tell me the sum of unit sold grouped by customer','','',''),
  (291,'Tell me the sum of unit sold grouped by customer','','',''),
  (292,'sum unit by customer','','',''),
  (293,'sales by customer for year greater 2018','','','');