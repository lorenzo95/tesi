package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.ResultSet;
import java.sql.Statement;

import org.junit.Test;

import com.google.common.collect.Lists;

import it.unibo.conversational.algorithms.utils.TabTable;
import it.unibo.conversational.database.DBmanager;
import it.unibo.conversational.database.DBsynonyms;
import it.unibo.conversational.database.QueryGeneratorChecker;

/**
 * Test database connection.
 */
public class TestConnection {

  private void count(final String table) throws Exception {
    try (
        Statement stmt = DBmanager.getMetaConnection().createStatement();
        ResultSet res = stmt.executeQuery("select * from `" + table + "`")
    ) {
      assertTrue(table + " is empty", res.first());
    } catch (final Exception e) {
      e.printStackTrace();
      fail();
    }
  }

  /**
   * Test non empty tables.
   * @throws Exception in case of error
   */
  @Test
  public void testNonEmptyTables() throws Exception {
    count(TabTable.tabTABLE);
    count(TabTable.tabRELATIONSHIP);
    count(TabTable.tabCOLUMN);
    count(TabTable.tabDATABASE);
    count(TabTable.tabFACT);
    count(TabTable.tabHiF);
    count(TabTable.tabHIERARCHY);
    count(TabTable.tabLEVEL);
    count(TabTable.tabMEMBER);
    count(TabTable.tabMEASURE);
    count(TabTable.tabGROUPBYOPERATOR);
    count(TabTable.tabSYNONYM);
    count(TabTable.tabLANGUAGEPREDICATE);
    count(TabTable.tabGRBYOPMEASURE); // MUST BY POPULATED MANUALLY
    // count(DBmanager.tabLEVELROLLUP); MUST BY POPULATED MANUALLY
  }

  /**
   * Test functions.
   * @throws Exception in case of error
   */
  @Test
  public void testFunctions() {
    try {
      assertFalse(QueryGeneratorChecker.getOperatorOfMeasure().isEmpty());
      assertFalse(QueryGeneratorChecker.getMembersOfLevels().isEmpty());
      assertFalse(QueryGeneratorChecker.getLevelsOfMembers().isEmpty());
      assertFalse(QueryGeneratorChecker.getYearLevels().isEmpty());
      QueryGeneratorChecker.getLevel("the_year");
      DBsynonyms.getEntities(Lists.newLinkedList(), 1.0, 1.0, 1, 1);
    } catch (final Exception e) {
      e.printStackTrace();
      fail();
    }
  }
}
