package test;

import static org.junit.Assert.assertEquals;

import java.util.stream.Collectors;

import org.junit.Test;

import it.unibo.conversational.Validator;
import it.unibo.conversational.algorithms.Parser;
import it.unibo.conversational.datatypes.Mapping;

/** Test the validation accuracy. */
public class TestAmbiguity {

  /** Test disambiguation.
   * @throws Exception in case of error 
   */
  @Test
  public void test1() throws Exception {
    test("sum unit sales by media type for USA", "media_type", "country = USA", "sum unit_sales", 1);
  }

  /** Test disambiguation.
   * @throws Exception in case of error 
   */
  @Test
  public void test10() throws Exception {
    test("sum unit sales by media type for USA country", "media_type", "country = USA", "sum unit_sales", 0);
  }

  /** Test disambiguation.
   * @throws Exception in case of error 
   */
  @Test
  public void test2() throws Exception {
    test("sum unit sales by media type for USA and Mexico", "media_type", "country = USA and country = Mexico", "sum unit_sales", 2);
  }

  /** Test disambiguation.
   * @throws Exception in case of error 
   */
  @Test
  public void test3() throws Exception {
    test("sum unit sales by media type for Salem", "media_type", "city = Salem", "sum unit_sales", 1);
  }

  /** Test disambiguation.
   * @throws Exception in case of error 
   */
  @Test
  public void test4() throws Exception {
    test("unit sales for USA", "", "country = USA", "avg unit_sales", 2);
  }

  /** Test disambiguation.
   * @throws Exception in case of error 
   */
  @Test
  public void test5() throws Exception {
    test("unit sales for USA and state province Sheri Nowmere", "", "country = USA and state_province = BC", "avg unit_sales", 3);
  }

  /** Test disambiguation.
   * @throws Exception in case of error 
   */
  @Test
  public void test6() throws Exception {
    test("unit sales for USA and Sheri Nowmere as province", "", "country = USA and state_province = BC", "avg unit_sales", 3);
  }

//  /** Test disambiguation. */
//  @Test
//  public void test7() throws Exception {
//    test("unit sales by month in 2010 for Atomic Mints USA by store", "the_month, store_id", "the_year = 2010 and product_name = Atomic Mints and country = USA", "avg unit_sales");
//  }

  /** Test disambiguation.
   * @throws Exception in case of error 
   */
  @Test
  public void test8() throws Exception {
    test("unit sales in 2010 and Atomic Mints", "", "the_year = 2010 and product_name = Atomic Mints", "avg unit_sales", 1);
  }

  /** Test disambiguation.
   * @throws Exception in case of error 
   */
  @Test
  public void test9() throws Exception {
    test("unit sales for Sheri Nowmere", "", "fullname = Sheri Nowmer", "avg unit_sales", 1);
  }

  private void test(final String nl, final String gc, final String sc, 
		  final String mc, final int expectedAnnotations) throws Exception {
    final Mapping ambiguousMapping = Validator.parseAndTranslate(nl, 1).get(0).getLeft();
    assertEquals(ambiguousMapping.getAnnotatedNgrams().stream().map(n -> n.getAnnotations()).collect(Collectors.toList()).toString(), expectedAnnotations, ambiguousMapping.getAnnotatedNgrams().size());
    final Mapping correctSentence = Validator.getBest(gc, sc, mc);
    // System.out.println(ambiguousMapping.toJSON());
    for (int i = 0; i < 4; i++) {
      Parser.disambiguate(ambiguousMapping);
    }
    assertEquals(1.0, correctSentence.similarity(ambiguousMapping), 0.001);
  }
}
