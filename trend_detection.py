# -*- coding: utf-8 -*-
"""
Created on Tue Dec 22 17:43:58 2020

@author: Lorenzo
"""

import matplotlib.pyplot as plt
import numpy as np

namefiles = ['OpenCSVWriteByResultSet0.csv', 'OpenCSVWriteByResultSet1.csv']
labels = []
datas = []

for nameFile in namefiles:
    file = open(nameFile,'r')
    data = []
    label = []
    while True:
    		line = file.readline()
    		if not line:
    			break
    		else:
    			spl = line.split(",")
    			for string in spl:
    				lun = len(string)-2
    				valString = string[1:lun]
    				valFloat = -1.00
    				try:
    					valFloat = float(valString)
    				except:
    					label.append(valString)
    				finally:
    					if (valFloat > 0):
    						data.append(valFloat)
    file.close()
    labels.append(label)
    datas.append(data)
    
    #da finire
def findMax(myList):
	max = -1.00
	for elem in myList:
		if (elem > max):
			max = elem
	return max

plt.figure()

sizeDatas = len(datas)
numPlot = 211
for i in range(0, sizeDatas):
	#plt.figure(figsize=(20, 1))
	plt.subplot(numPlot + i)
	plt.plot(datas[i])
	plt.axis([0, len(datas[i]), 0, findMax(datas[i])])

	#plt.bar(label, data)

	plt.ylabel('values')
	plt.title(labels[i])

plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.3,wspace=0.35)
plt.show()

maxLen = -1
for i in range(0, sizeDatas):
	if (len(datas[i]) > maxLen):
		maxLen = len(datas[i])

#trend = []
trend = np.array(range(0, maxLen))
for i in range(0, maxLen):
	diff = datas[0][i]
	for j in range(1, sizeDatas):
		try:
			valTemp = datas[j][i]
		except:
			print()
		finally:
			if (valTemp > 0 and diff > 0):
				diff -= valTemp
				#trend.append(diff)
				trend[i] = diff

axisX = np.array(range(0, len(trend)))
plt.plot(axisX, trend, 'b--')
plt.ylabel('value')
plt.title('trend')
plt.show()