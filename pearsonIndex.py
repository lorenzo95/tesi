# generate related variables
from numpy import cov
from matplotlib import pyplot
from scipy.stats import pearsonr
import pandas as pd
import moduleRead as mR

# prepare data
df = pd.read_csv('OpenCSVWriteByResultSet0.csv', sep=',')

val_vars = mR.readFile()
listData = []
for val in val_vars:
    listData.append(df[val])

data1 = df[val_vars[0]]
data2 = df[val_vars[1]]

# plot
pyplot.scatter(data1, data2)
pyplot.show()

# calculate covariance matrix
covariance = cov(data1, data2)

# calculate Pearson's correlation
corr, _ = pearsonr(data1, data2)

mR.fileClean()
mR.writeFile('covariance ', 'correlation ', covariance, corr)