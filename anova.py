# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 11:11:06 2020

@author: Lorenzo
"""
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import scipy.stats as stats
import statsmodels.api as sm
from statsmodels.formula.api import ols
from bioinfokit.analys import stat
import moduleRead as mR

val_vars = mR.readFile()
# load data file
df = pd.read_csv('OpenCSVWriteByResultSet0.csv', sep=',')
df_melt = pd.melt(df.reset_index(), id_vars=['index'], value_vars=val_vars)
# replace column names
df_melt.columns = ['index', 'measure', 'value']

ax = sns.boxplot(x='measure', y='value', data=df_melt, color='#99c2a2')
plt.show()
fvalue, pvalue = stats.f_oneway(df[val_vars[0]], df[val_vars[1]])

mR.fileClean()
mR.writeFile('fvalue ', 'pvalue ', fvalue, pvalue)

# Ordinary Least Squares (OLS) model
model = ols('value ~ C(measure)', data=df_melt).fit()
anova_table = sm.stats.anova_lm(model, typ=2)
print(anova_table)

# ANOVA table using bioinfokit v1.0.3 or later (it uses wrapper script for anova_lm)
res = stat()
res.anova_stat(df=df_melt, res_var='value', anova_model='value ~ C(measure)')
print(res.anova_summary)

# QQ-plot
# res.anova_std_residuals are standardized residuals obtained from ANOVA (check above)
sm.qqplot(res.anova_std_residuals, line='45') # line='45' | 'q' | 'r'
plt.xlabel("Theoretical Quantiles")
plt.ylabel("Standardized Residuals")
plt.show()

# histogram
plt.hist(res.anova_model_out.resid, bins='auto', histtype='bar', ec='k') 
plt.xlabel("Residuals")
plt.ylabel('Frequency')
plt.show()