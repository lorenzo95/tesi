# identify outliers with standard deviation
import sys
from numpy import mean
from numpy import std

sizeParam = len(sys.argv)
for numArg in range(1, sizeParam):
	valFile = sys.argv[numArg]
	
	#open file info in read mode
	#file = open('OpenCSVWriteByResultSet0.csv','r')
	file = open(valFile,'r')
	data = []
	label = []

	line = file.readline()

	while True:
		line = file.readline()
		if not line:
			break
		else:
			spl = line.split(",")
			for string in spl:
				lun = len(string)-2
				valString = string[1:lun]
				valFloat = -1.00
				try:
					valFloat = float(valString)
				except:
					label.append(valString)
				finally:
					if (valFloat > 0):
						data.append(valFloat)
	file.close()

	# calculate summary statistics
	data_mean, data_std = mean(data), std(data)

	# identify outliers
	cut_off = data_std * 3
	lower = data_mean - cut_off
	upper = data_mean + cut_off

	# identify outliers
	outliers = [x for x in data if x < lower or x > upper]

	# remove outliers
	outliers_removed = [x for x in data if x >= lower and x <= upper]

	valFileOutPut = 'Dati_Out' + str(numArg-1) + '.txt'

	fileClean = open(valFileOutPut,'w')
	fileClean.write('')
	fileClean.close()

	file1 = open(valFileOutPut,'a')
	file1.write('media %.3f' % (mean(data)) + '\n')
	file1.write('stdv %.3f' % (std(data)) + '\n')

	file1.write('Outliers \n')
	for outlier in outliers:
		file1.write('outlier %.3f' % outlier + '\n')
	file1.close()