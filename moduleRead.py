# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 12:19:12 2021

@author: Lorenzo
"""
def fileClean() :
    fileClean = open('Dati_Out0.txt','w')
    fileClean.write('')
    fileClean.close()

def readFile() :       
    file = open('OpenCSVWriteByResultSet0.csv','r')
    line = file.readline()
    spl = line.split(',')
    file.close()
    
    val_vars = []
    size = len(spl) -1 #livello del group by
    for i in range(0, size):
        lun = -1
        valString = spl[i][1:lun]
        val_vars.append(valString)
    return val_vars

def writeFile(label1, label2, data1, data2):
    file = open("Dati_Out0.txt","a")
    file.write(label1 + str(data1))
    file.write('\n')
    file.write(label2 + str(data2))
    file.close()